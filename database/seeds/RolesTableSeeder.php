<?php

use Illuminate\Database\Seeder;

/**
 * Class RolesTableSeeder
 */
class RolesTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        DB::table('roles')->delete();

        DB::table('roles')
            ->insert([
                [
                    'name'       => 'Owner',
                    'names'      => 'Owners',
                    'name_trim'  => 'owner',
                    'name_trims' => 'owners',
                ],
                [
                    'name'       => 'Administrator',
                    'names'      => 'Administrators',
                    'name_trim'  => 'admin',
                    'name_trims' => 'admins',
                ],
                [
                    'name'       => 'Employee',
                    'names'      => 'Employees',
                    'name_trim'  => 'employee',
                    'name_trims' => 'employees',
                ],
            ]);
    }
}
