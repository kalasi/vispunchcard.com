<?php

use Illuminate\Database\Seeder;

/**
 * Class SiteTableSeeder
 */
class SiteTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        DB::table('site')->delete();

        DB::table('site')
            ->insert([
                [
                    'key'   =>'version',
                    'value' =>'0.7.0',
                ],
            ]);
    }
}
