<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBusinessesTable
 */
class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('city');
            $table->string('municipality');
            $table->string('country');
            $table->string('pin');
            $table->string('punches_required');
            $table->string('owner');
            $table->string('punches');
            $table->string('punchcards_used');
            $table->string('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('businesses');
    }
}
