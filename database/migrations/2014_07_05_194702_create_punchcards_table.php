<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePunchcardsTable
 */
class CreatePunchcardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punchcards', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user');
            $table->integer('business');
            $table->integer('punches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('punchcards');
    }
}
