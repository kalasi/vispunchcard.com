@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <div class='rollover-link'>
        <span>
            VisPunchcard
        </span>
    </div>
</div>
<div class='wrapper'>
    <h3 class='text-center fa-2x text-success'>
        Available For All Devices
    </h3>
    <div class='row text-center text-info'>
        <div class='col-xs-12 col-sm-4'>
            <i class='fa fa-laptop fa-5x'></i>
            <br />
            <span class='fa-2x'>
                Desktop
            </span>
        </div>
        <div class='col-xs-12 col-sm-4'>
            <i class='fa fa-tablet fa-5x'></i>
            <br />
            <span class='fa-2x'>
                Tablet
            </span>
        </div>
        <div class='col-xs-12 col-sm-4'>
            <i class='fa fa-mobile fa-5x'></i>
            <br />
            <span class='fa-2x'>
                Mobile
            </span>
        </div>
    </div>
</div>
<div class='wrapper'>
    <h3 class='text-center fa-2x text-success'>
        A Simple 3-Step Process
    </h3>
    <div id='home-process' class='row'>
        <div class='col-xs-12 col-md-4'>
            <p>
                Enter the customer's phone number.
            </p>
            <div class='form-group'>
                <label for='home-s1-p1'>
                    Phone Number
                </label>
                <div class='col-sm-10'>
                    <input type='tel' name='home-s1-p1' id='phone' pattern='[\+][\(]d{3}[\)] d{3}[\-]d{4}' placeholder='(987) 654-3210' autocomplete='off' />
                </div>
            </div>
        </div>
        <div class='col-xs-12 col-md-4'>
            <p>
                Enter the punches to add to the punchcard and number of full punchcards to use.
            </p>
            <div class='form-group'>
                <label for='home-s2-p1'>
                    New Punches Today
                </label>
                <div class='col-sm-10'>
                    <input type='number' id='home-s2-p1' placeholder='ex: 4' min='0' />
                </div>
            </div>
            <div class='form-group'>
                <label for='home-s2-p2'>
                    Punchcards Used Today
                </label>
                <div class='col-sm-10'>
                    <input type='number' id='home-s2-p2' placeholder='ex: 2' />
                </div>
            </div>
        </div>
        <div class='col-xs-12 col-md-4'>
            <p>
                Complete the order, showing how many punches they have now.  You can ask for their Email Address if you run promotional offers.
            </p>
            <div class='form-group'>
                <label for='home-s3-p1'>
                    Sign up for promotional offers!
                </label>
                <div class='col-sm-10'>
                    <input type='email' id='home-s3-p1' placeholder='john@example.com' />
                </div>
            </div>
        </div>
    </div>
</div>
<div class='wrapper'>
    <h3 class='text-center fa-2x text-success'>
        Get Started Today
    </h3>
    <div class='row'>
        <div class='col-xs-12 col-sm-6'>
            <div id='home-started-signup' class='rollover-link'>
                <a href='{{ url('signup') }}' title='Sign Up'>
                    Sign Up
                </a>
            </div>
        </div>
        <div class='col-xs-12 col-sm-6'>
            <div id='home-started-login' class='rollover-link'>
                <a href='{{ url('login') }}' title='Login'>
                    Login
                </a>
            </div>
        </div>
    </div>
</div>
@stop
