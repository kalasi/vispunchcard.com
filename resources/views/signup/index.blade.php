@extends('layouts.form')
@section('content')
<div class='row wrapper'>
    <form class='form-horizontal' action='' method='post' role='form'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <em rt-header='Sign Up'></em>
        <div>
            <em rt-bind='phone' rt-required='1'></em>
            <input type='tel' name='phone' minlength='14' maxlength='14' />
            <span>
                Phone Number
            </span>
            <p>
                We'll automatically format your phone number.
            </p>
            <p>
                That is not a proper US phone number.
            </p>
        </div>
        <div>
            <em rt-bind='name' rt-required='1' rt-enter='1'></em>
            <input type='text' name='fname' />
            <span>
                First Name
            </span>
            <p>
                Press Enter when done.
            </p>
            <p>
                You haven't filled out your name.
            </p>
        </div>
    </form>
</div>
<script>
    var Form=new Form();
    Form.setup();
</script>
@stop
