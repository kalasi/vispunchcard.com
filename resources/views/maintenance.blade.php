@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='text-danger'>
        VisPunchcard is currently under planned maintenance.
    </p>
    <p class='text-info'>
        We will be back within a couple of minutes.
    </p>
</div>
@stop