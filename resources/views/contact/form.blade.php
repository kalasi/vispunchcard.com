@extends('layouts.form')
@section('content')
<div class='row wrapper'>
    <form class='form-horizontal' action='' method='post' role='form'>
        <em rt-header='Contact Us'></em>
        <div>
            <em rt-bind='email' rt-required='1' rt-enter='1'></em>
            <input type='email' name='email' />
            <span>
                Email Address
            </span>
            <p>
                We'll be using this Email Address to reply to you.
            </p>
            <p>
                That is not a valid Email Address.
            </p>
        </div>
        <div>
            <em rt-bind='name' rt-required='1' rt-enter='1'></em>
            <input type='text' name='fname' />
            <span>
                First Name
            </span>
            <p>
                Only your first name, not your last name.
            </p>
            <p>
                You haven't entered your First Name.
            </p>
        </div>
        <div>
            <em rt-bind='name' rt-required='1' rt-enter='1'></em>
            <input type='text' name='subject' />
            <span>
                Subject
            </span>
            <p>
                Press Enter when done.
            </p>
            <p>
                You haven't entered a Subject line.
            </p>
        </div>
        <div>
            <em rt-bind='name' rt-required='1' rt-enter='1'></em>
            <textarea name='contents'></textarea>
            <span>
                Message
            </span>
            <p>
                Press the enter button to the right when done.
            </p>
            <p>
                You haven't entered a message.
            </p>
        </div>
    </form>
</div>
<script>
    var Form = new Form();
    Form.setup();
</script>
@stop
