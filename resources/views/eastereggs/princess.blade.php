@extends('layouts.default')
@section('content')
<div class='row wrapper text-center'>
    Sorry, <b>{{ $name }}</b> is in another castle. :c
</div>
@stop
