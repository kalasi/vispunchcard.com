@extends('layouts.form')
@section('content')
<div class='row wrapper'>
    <form class='form-horizontal' action='' method='post' role='form'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <em rt-header='Login'></em>
        <div>
            <em rt-bind='phone'></em>
            <input type='tel' name='phone' />
            <span>
                Owner's Phone Number
            </span>
            <p>
                We'll automatically format your phone number.
            </p>
            <p>
                It appears as though that is not a proper US phone number.
            </p>
        </div>
        <div>
            <em rt-bind='name'></em>
            <input type='text' name='name' />
            <span>
                Business Name
            </span>
            <p>
                Please enter the name of the business (<span class='text-danger'>capitalization matters!</span>)
            </p>
            <p>
                A name was not entered.
            </p>
        </div>
        <div>
            <em rt-bind='pin'></em>
            <input type='number' name='pin' />
            <span>
                Business 4-Digit PIN
            </span>
            <p>
                To continue, please enter your 4-digit business PIN.  This is a one-time process for checkout.
            </p>
            <p>
                That is not a valid 4-digit PIN number!
            </p>
        </div>
    </form>
</div>
<script>
    var Form=new Form();
    Form.setup();
</script>
@stop
