@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <div class='row'>
        <div class='col-xs-12 col-sm-6'>
            <div class='rollover-link'>
                <a href='{{ url('login/user') }}' title='User'>
                    User
                </a>
            </div>
        </div>
        <div class='col-xs-12 col-sm-6'>
            <div class='rollover-link'>
                <a href='{{ url('login/business') }}' title='Business'>
                    Business
                </a>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var loginH1 = $('.rollover-link a').eq(0).width();
        var loginH2 = $('.rollover-link a').eq(1).width();
        var loginHM = Math.max(loginH1, loginH2);
        $('.rollover-link a').width(loginHM);
    });
</script>
@stop
