@extends('layouts.form')
@section('content')
<div class='row wrapper'>
    <form class='form-horizontal' action='' method='post' role='form'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <em rt-header='Login'></em>
        <div>
            <em rt-bind='phone'></em>
            <input type='tel' name='phone' />
            <span>
                Phone Number
            </span>
            <p>
                We'll automatically format your phone number.  Alternatively, you may login as a business if you're a business owner.
                <br />
                <a href='{{ url('login/business') }}' class='btn btn-info btn-lg' role='button'>
                    Login as Business
                </a>
            </p>
            <p>
                It appears as though that is not a proper US phone number.
            </p>
        </div>
    </form>
</div>
<script>
    var Form=new Form();
    Form.setup();
</script>
@stop
