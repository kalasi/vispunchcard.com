@extends('layouts.form')
@section('content')
<div class='row wrapper'>
    <form class='form-horizontal' action='' method='post' role='form'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <em rt-header='Sign In to {{ $business->name }}'></em>
        <div>
            <em rt-bind='phone'></em>
            <input type='tel' name='phone' />
            <span>
                Employee Phone Number
            </span>
            <p>
                We'll automatically format your phone number.
            </p>
            <p>
                It appears as though that is not a proper US phone number.
            </p>
        </div>
    </form>
</div>
<script>
    var Form = new Form();
    Form.setup();
</script>
@stop
