@extends('layouts.default')
@section('content')
<div class='wrapper'>
@if(isset($signedIn))
    {{ $signedIn->fname }} is currently signed in for checkout.
    <ul class='list-inline inline'>
        <li>
            <a href='{{ Utilities::URLBusiness($business->id) }}' title='Checkout'>
                <i class='fa fa-shopping-cart'></i> Checkout
            </a>
        </li>
        <li>
            <a href='{{ Utilities::URLBusiness('business/sign-out') }}' title='Sign Out'>
                <i class='fa fa-key'></i> Sign Out
            </a>
        </li>
    </ul>
@endif
@foreach($staffList as $staff)
    <div class='well well-dark'>
        <h2>
            {{ $staff->name }}
        </h2>
        <ul class='list-inline'>
            <li>
                <a href='{{ url('business/sign-in/' . $staff->id) }}' title='Checkout'>
                    <i class='fa fa-shopping-cart'></i> Checkout
                </a>
            </li>
        </ul>
    </div>
@endforeach
</div>
@stop
