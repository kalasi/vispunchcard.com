@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='text-success'>
        You have been logged out of your account.
    </p>
    <p>
        <a href='{{ url('login') }}' class='btn btn-primary' role='button'>
            Login
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-primary' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
