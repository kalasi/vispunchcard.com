@extends('layouts.default')
@section('content')
<div class='row wrapper text-center'>
    <h1 class='text-rt'>
        VisPunchcard
    </h1>
    <h4 class='text-primary'>
        {{ $business->name }}
    </h4>
    <p class='text-primary'>
        <small>
            <em>
                {{ $business->city }}, {{ $business->municipality }}
            </em>
        </small>
    </p>
    <h2>
        Current Punchcard
    </h2>
    <div class='well punchcard'>
        <h4>
            {{ $business->name }}
        </h4>
        <p>
            {{ $business->city }}, {{ $business->municipality }}
        </p>
@for($x = 1; $x <= $full; $x++)
    @for($i = 1; $i <= $business->punches_required; $i++)
        <span class='punch'></span>
    @endfor
        <span class='text-success fa fa-check'></span>
        <br />
@endfor
@for($x = 1; $x <= $current; $x++)
        <span class='punch'></span>
@endfor
@for($x = $current; $x < $business->punches_required; $x++)
        <span class='punch-empty'></span>
@endfor
        <span>
            {{ $customer->punches % $business->punches_required }} / {{ $business->punches_required }}
        </span>
    </div>
    <h3 class='text-primary'>
        Information
    </h3>
    <h5>
        Full Punchcards:
    </h5>
    <p>
        {{ floor($customer->punches / $business->punches_required) }}
    </p>
    <form action='{{ Utilities::URLBusiness($business->id) }}' method='post' role='form' class='col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <input type='hidden' name='user_id' value='{{ $user->id }}' />
@if(empty($customer->email))
        <p class='text-info'>
            Sign up for promotional offers!
        </p>
        <input type='email' name='email' placeholder='john@example.com' />
@else
        <p class='text-success'>
            {{ $user->fname }} already has their email address signed up.
        </p>
@endif
        <br />
        <p>
            <button class='btn btn-info' type='submit'>
                Complete
            </button>
        </p>
    </form>
</div>
@stop
