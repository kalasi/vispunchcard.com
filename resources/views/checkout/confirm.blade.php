@extends('layouts.default')
@section('content')
<div class='wrapper text-center'>
    <p class='text-left'>
        <a href='{{ Utilities::URLBusiness($business->id) }}' class='text-rt'>
            <i class='fa fa-refresh fa-spin'></i> New Entry
        </a>
    </p>
    <h1 class='text-rt'>
        VisPunchcard
    </h1>
    <h4 class='text-primary'>
        {{ $business->name }}
    </h4>
    <p class='text-primary'>
        <small>
            <em>
                {{ $business->city }}, {{ $business->municipality }}
            </em>
        </small>
    </p>
    <form id='business-form' action='{{ Utilities::URLBusiness($business->id, 'complete') }}' method='post' role='form' class='row'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <input type='hidden' name='user_id' value='{{ $user->id }}' />
        <div class='col-xs-12 col-sm-6'>
            <div class='well well-dark'>
                <h3 class='text-info'>
                    Customer Info
                </h3>
                <dl>
                    <dt class='text-info'>
                        Name
                    </dt>
                    <dd>
                        {{ $user->fname }}
                    </dd>
                    <dt class='text-info'>
                        Punchcards Full
                    </dt>
                    <dd id='punchcards-full'>
                        {{ $full }}
                    </dd>
                </dl>
                <h3>
                    Punchcard
                </h3>
                <div class='well punchcard'>
                    <h4>
                        {{ $business->name }}
                    </h4>
                    <p>
                        {{ $business->city }}, {{ $business->municipality }}
                    </p>
                    <div>
@for($x = 1; $x <= $punchesCurrent; $x++)
                        <span class='punch'></span>
@endfor
@for($x = $punchesCurrent; $x < $business->punches_required; $x++)
                        <span class='punch-empty'></span>
@endfor
                    </div>
                    <small>
                        <span>{{ $punchesCurrent }}</span>/<span>{{ $business->punches_required }}</span>
                    </small>
                    <em rt-started='{{ $punchesCurrent }}' rt-per='{{ $business->punches_required }}'></em>
                </div>
            </div>
        </div>
        <div class='col-xs-12 col-sm-6'>
            <div class='well well-dark'>
                <h3 class='text-info'>
                    Punchcard Info
                </h3>
                <h4 class='text-success'>
                    New Punches Today
                </h4>
                <input type='number' name='new' placeholder='ex: 4' min='0' required />
                <h4 class='text-success'>
                    Free Used Today
                </h4>
                <input type='number' name='used' placeholder='ex: {{ $full }}' />
                <p id='used-error'>
                    <i class='fa fa-warning'></i> That's <span>0</span> too many punchcards!
                </p>
                <button class='btn btn-info' type='submit'>
                    Complete
                </button>
            </div>
        </div>
    </form>
</div>
@stop
