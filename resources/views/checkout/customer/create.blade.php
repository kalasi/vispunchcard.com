@extends('layouts.default')
@section('content')
<div class='row wrapper'>
    <form class='text-center' action='{{ Utilities::URLBusiness($business->id, 'create') }}' method='post' role='form'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <input type='hidden' name='data' value='{{$phone}}' />
        <p class='text-info'>
            <em>
                Please note that businesses have no access to your phone number and your phone number will <u>NEVER</u> be used by VisPunchcard.
            </em>
        </p>
        <h2>
            First Name
        </h2>
        <input id='fname' type='text' name='fname' placeholder='John' required spellcheck='false' />
        <p>
            <em class='text-warning'>Please enter only your first name.</em>
        </p>
        <p>
            <button class='btn btn-info' type='submit'>
                Sign Up
            </button>
        </p>
    </form>
</div>
@stop
