@extends('layouts.form')
@section('content')
<div class='row wrapper'>
    <form id='login' class='form-horizontal' action='' method='post' role='form'>
        <em rt-header='Business PIN'></em>
        <div>
            <em rt-bind='pin' rt-required='1'></em>
            <input type='number' name='pin' />
            <span>
                4-Digit PIN
            </span>
            <p>
                To continue, please enter your 4-digit business PIN.  This is a one-time process for checkout.
            </p>
            <p>
                That is not a valid 4-digit PIN number!
            </p>
        </div>
    </form>
</div>
<script>
    var Form = new Form();
    Form.setup();
</script>
@stop
