@extends('layouts.default')
@section('content')
<div class='row wrapper text-center'>
    <h1 class='text-rt'>
        VisPunchcard
    </h1>
    <h4 class='text-primary'>
        {{ $business->name }}
    </h4>
    <p class='text-primary'>
        <small>
            <em>
                {{ $business->city }}, {{ $business->municipality }}
            </em>
        </small>
    </p>
    <form action='{{ Utilities::URLBusiness($business->id, 'confirm') }}' method='post' role='form' class='col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <h2 class='text-center'>
            Phone Number
        </h2>
        <input type='tel' name='phone' id='phone' pattern='[\+][\(]d{3}[\)] d{3}[\-]d{4}' placeholder='(987) 654-3210' autocomplete='off' />
        <br />
        <span class='help-block'>
            We'll automatically format your phone number.
        </span>
    </form>
</div>
<script>
    var PhoneForm = new PhoneForm();
    PhoneForm.setup();
</script>
@stop
