<?php
$navsLeft = [
	''        => 'Home',
	'about'   => 'About',
	'contact' => 'Contact',
];
if (User::$logged) {
	$navsRight = [
		'manage'  => 'Manage',
		'account' => 'Account',
		'logout'  => 'Logout',
	];
} else {
	$navsRight = [
		'login'  => 'Login',
		'signup' => 'Sign Up',
	];
}

$mobile = Utilities::mobile() ? "var MOBILE=1;" : "";
if(!isset($bc))                $bc = array();
if(!isset($displayPageHeader)) $displayPageHeader = true;
if(!empty($title))             $bc['#'] = $title;
?>
<!DOCTYPE html>
<html class='no-js'>
	<head>
@if(App::environment() != 'local')
		<script>
			startTime = new Date().getTime();
		</script>
@endif
		<base href='/' />
		<title>
			{{ !empty($title) ? $title . " | " : "" }}VisPunchcard
		</title>
		<meta charset='UTF-8' />
		<meta http-equiv='Content-Type' content='text/html;charset=iso-8859-1' />
		<meta name='author' content='Austin Hellyer' />
		<meta name='description' content='VisPunchcard: The Online Solution for Punchcards' />
		<meta name='Generator' content='VisPunchcard' />
		<meta name='keywords' content='' />
		<meta name='robots' content='index,follow' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />
		<link rel='apple-touch-icon' href='/css/favicon.png' />
		<link rel='canonical' href='http://vispunchcard.com/' />
		<link rel='home' href='/' />
		<link rel='index' href='/sitemap/' />
        <link rel='stylesheet' href='/css/style.css'>
        <link rel='stylesheet' href='/css/bootstrap.css'>
        <link rel='stylesheet' href='/css/bootstraptheme.css'>
        <link rel='stylesheet' href='/css/font-awesome.css'>
@if(!empty($css))
        <link rel='stylesheet' href='/css/{{ $css }}.css'>
@endif
	</head>
	<body>
        <script src='/js/jquery.js'></script>
        <script src='/js/main.js'></script>
        <script src='/js/bootstrap.js'></script>
        <script src='/js/vispunchcard.js'></script>
@if(!empty($js))
        <script src='/js/{{ $js }}.js'></script>
@endif
		<div id='page'>
@yield('content') 
		</div>
@if(!App::environment('local'))
		<script>
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-52568813-1']);
            _gaq.push(['_trackPageview']);
            (function() {
                var endTime=new Date().getTime();
                var timeSpentGA=endTime-startTime;
                _gaq.push(['_trackTiming','Test','Page Load',timeSpentGA,'animation',100]);
                console.log(timeSpentGA);
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
		</script>
@endif
	</body>
</html>
