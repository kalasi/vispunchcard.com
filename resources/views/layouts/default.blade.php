<?php
$navsLeft = [
	''        => 'Home',
	'about'   => 'About',
	'contact' => 'Contact',
];

if (\User::$logged) {
	$navsRight = [
		'manage' => 'Manage',
		\Auth::user()->name() => [
			'account'            => "<i class='fa fa-user'></i> View Account",
			'account/punchcards' => "<i class='fa fa-credit-card'></i> My Punchcards",
			'account/settings'   => "<i class='fa fa-cog'></i> Settings",
		],
		'logout' => 'Logout',
	];
} elseif (\Business::isBusiness()) {
	$navsRight = [
		'business' => \Business::info(\Business::cookie()->id)->name,
	];
	if (\Business::signedIn()) {
		$signedInfo = \Business::signedInInfo();
		$navsRight['business/sign-out'] = 'Sign Out of ' . $signedInfo->fname;
	} else {
		$navsRight['logout'] = 'Logout';
	}
} else {
	$navsRight = [
		'login'  => 'Login',
		'signup' => 'Sign Up',
	];
}
$validNavs=[
	'Home',
	'About',
	'Contact',
	'Manage',
	'Account',
	'Logout',
	'Login',
	'Sign Up'
];
$mobile = \Utilities::mobile() ? "var MOBILE=1;" : "";
if (!isset($bc))                $bc = array();
if (!isset($css))               $css = '';
if (!isset($displayPageHeader)) $displayPageHeader = true;
if (!isset($js))                $js = '';
if (!isset($nav))               $nav = "Home";
if (!empty($title))             $bc['#'] = $title;
$current = $nav;

/**
 * Controller of nav light in series
 */
$valid = false;
foreach ($navsLeft as $index => $value) {
	if ($nav == $index || $nav == $value) {
		$valid = true;
	}
}

foreach ($navsRight as $index => $value) {
	if ($nav == $index || $nav == $value) {
		$valid = true;
	}
}

if (!$valid) {
	if (!isset($navURL)) $navURL = substr($_SERVER['REQUEST_URI'], 1);
}
?>
<!DOCTYPE html>
<html class='no-js'>
	<head>
@if (\App::environment() != 'local')
		<script>
			startTime = new Date().getTime();
		</script>
@endif
		<base href='/' />
		<title>
			{{ !empty($title) ? $title . " | " : "" }}VisPunchcard
		</title>
		<meta charset='UTF-8' />
		<meta http-equiv='Content-Type' content='text/html;charset=iso-8859-1' />
		<meta name='author' content='Austin Hellyer' />
		<meta name='description' content='VisPunchcard: The Online Solution for Punchcards' />
		<meta name='Generator' content='VisPunchcard' />
		<meta name='keywords' content='' />
		<meta name='robots' content='index,follow' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />
		<meta name='msapplication-TileColor' content='#821083'>
		<meta name='msapplication-TileImage' content='/img/apple-touch-icon-precomposed.png'>
		<link rel='apple-touch-icon-precomposed' href='/img/apple-touch-icon-precomposed.png'>
		<link rel='icon' href='/img/favicon.png' />
		<link rel='shortcut icon' href='/img/favicon.ico' />
		<link rel='canonical' href='http://vispunchcard.com/' />
		<link rel='home' href='/' />
		<link rel='index' href='/sitemap/' />
        <link rel='stylesheet' href='/css/style.css'>
        <link rel='stylesheet' href='/css/bootstrap.css'>
        <link rel='stylesheet' href='/css/bootstraptheme.css'>
        <link rel='stylesheet' href='/css/font-awesome.css'>
        <link rel='stylesheet' href='/css/jquery-ui.css'>
@if(!empty($css))
        <link rel='stylesheet' href='/css/{{ $css }}.css'>
@endif
	</head>
	<body>
        <script src='/js/jquery.js'></script>
        <script src='/js/jquery-ui.js'></script>
        <script src='/js/{{ $js }}.js'></script>
@if(!empty($js))
        <script src='/js/{{ $js }}.js'></script>
@endif
		<nav class='navbar navbar-default navbar-fixed-top navbar-inverse navbar-custom' role='navigation'>
			<div class='container-fluid'>
				<div class='navbar-header'>
					<button class='navbar-toggle' type='button' data-toggle='collapse' data-target='#navbar-ul'>
						<span class='sr-only'>
							Toggle Navigation
						</span>
						<span class='icon-bar'></span>
						<span class='icon-bar'></span>
						<span class='icon-bar'></span>
					</button>
                    <a href='{{ url() }}' class='navbar-brand'>
                        VisPunchcard
                    </a>
				</div>
				<div id='navbar-ul' class='collapse navbar-collapse navbar-ex1-collapse'>
					<ul class='nav navbar-nav navbar-left'>
@foreach($navsLeft as $url => $name)
	@if(is_array($name))
						<li class='{{ $url == $nav ? "active " : ""}}dropdown'>
							<a href='#' class='dropdown-toggle' data-toggle='dropdown'>
								{{ $url }} <span class='caret'></span>
							</a>
							<ul class='dropdown-menu' role='menu'>
		@foreach($name as $url2 => $name2)
								<li>
                                    <a href='{{ url($url2) }}'>
                                        {!! $name2 !!}
                                    </a>
								</li>
		@endforeach
							</ul>
						</li>
	@else
						<li{{ $name == $current ? " class='active'" : "" }}>
                            <a href='{{ $url }}'>
                                {!! $name !!}
                            </a>
						</li>
	@endif
@endforeach
					</ul>
					<ul class='nav navbar-nav navbar-right'>
@foreach($navsRight as $url => $name)
	@if(is_array($name))
						<li class='{{ $url == $nav ? "active " : "" }}dropdown'>
							<a href='#' class='dropdown-toggle' data-toggle='dropdown'>
								{{ $url }} <span class='caret'></span>
							</a>
							<ul class='dropdown-menu' role='menu'>
		@foreach($name as $url2 => $name2)
								<li>
									<a href='{{ url($url2) }}'>
										{!! $name2 !!}
									</a>
								</li>
		@endforeach
							</ul>
						</li>
	@else
						<li{{ $name == $nav ? " class='active'" : "" }}>
                            <a href='{{ $url }}'>
                                {!! $name !!}
                            </a>
						</li>
	@endif
@endforeach
					</ul>
				</div>
			</div>
		</nav>
		<div id='page'>
@if($bc)
			<div class='wrapper'>
				<ol class='breadcrumb'>
					<li>
						<a href='{{ url() }}' title='Home'>
							Home
						</a>
					</li>
	@foreach($bc as $url => $name)
		@if($url == "#")
					<li class='active'>
						{{ $name }}
					</li>
		@else
					<li>
                        <a href='{{ $url }}'>
                            {{ $name }}
                        </a>
					</li>
		@endif
	@endforeach
				</ol>
@endif
@if($displayPageHeader && !empty($title))
				<div class='page-header'>
					<h1>
						{{ $title }} @if(isset($titleSub))<small>{{ $titleSub }}</small> @endif
					</h1>
				</div>
@endif
			</div>
@yield('content') 
		</div>
		<div id='portfolio' class='wrapper-small'>
			<h2>
				VisPunchcard
			</h2>
			<span class='byline'>
				The online solution to punchcards.
			</span>
		</div>
		<div id='copyright'>
			<p>
				&copy;{{ date('Y') }} VisPunchcard <span></span> Design by <a href='http://austinhellyer.me' target='_blank' title='Austin Hellyer'>Austin Hellyer</a>
			</p>
		</div>
@if(!App::environment('local'))
		<script>
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-52568813-1']);
            _gaq.push(['_trackPageview']);
            (function() {
                var endTime=new Date().getTime();
                var timeSpentGA=endTime-startTime;
                _gaq.push(['_trackTiming','Test','Page Load',timeSpentGA,'animation',100]);
                console.log(timeSpentGA);
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
		</script>
@endif
        <script src='/js/vispunchcard.js'></script>
	</body>
</html>
