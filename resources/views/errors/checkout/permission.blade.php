@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='text-danger'>
        You do not have permission to checkout for this business.
    </p>
    <p class='text-info'>
        You are either not employed by, are not an administrator of, or are not an owner of this business.
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
