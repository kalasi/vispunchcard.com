@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='lead text-danger'>
        This business does not exist!
    </p>
    <p class='text-info'>
        The business with an ID of <b>{{ $id }}</b> does not exist.  Either it was removed or taken down by the owner.
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
