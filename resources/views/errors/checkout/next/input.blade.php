@extends('layouts.default')
@section('content')
<div class='row wrapper'>
    <p class='lead text-danger'>
        There was an issue with finding your phone number.
    </p>
    <p class='text-info'>
        It seems the input was unreadable and was not in the following format: (NXX) NXX-XXXX, where N is a number greater than 2 and X is any number.
    </p>
    <p>
        <a href='{{ url(Utilities::URLBusiness($business->id)) }}' class='btn btn-info btn-lg' role='button'>
            Checkout Again
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
