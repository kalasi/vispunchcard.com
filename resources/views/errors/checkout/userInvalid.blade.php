@extends('layouts.default')
@section('content')
<div class='row wrapper'>
    <p class='lead text-danger'>
        There was an issue with determining the user.
    </p>
    <p class='text-info'>
        It would seem the user does not exist.  No idea how that happened.  Try again?
    </p>
    <p>
        <a href='{{ Utilities::URLBusiness($business->id) }}' class='btn btn-info btn-lg' role='button'>
            Back to Start
        </a>
    </p>
</div>
@stop
