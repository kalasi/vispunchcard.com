@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='lead text-danger'>
        Someone is already signed in.
    </p>
    <p class='text-info'>
        {{ $user->fname }} is already signed in.  To sign in they must logout.
    </p>
    <p>
        <a href='{{ url('business/sign-out') }}' class='btn btn-info btn-lg' role='button'>
            Sign Out
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
