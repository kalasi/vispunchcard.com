@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='lead text-danger'>
        The login credentials used are incorrect.
    </p>
    <p class='text-info'>
        The phone number used to access the account was incorrect.
    </p>
    <p>
        <a href='{{ url('business') }}' class='btn btn-info btn-lg' role='button'>
            Back to Business
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
