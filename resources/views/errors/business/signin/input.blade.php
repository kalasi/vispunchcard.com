@extends('layouts.default')
@section('content')
<div class='row wrapper'>
    <p class='lead text-danger'>
        There was an issue while creating your account.
    </p>
    <p class='text-info'>
        It seems you didn't fill out all of the fields correctly. The following are required:
    </p>
    <ul class='text-info'>
        <li>
            Phone Number (987) 654-3210:
            <ul>
                <li>
                    Area Code: <b>(987)</b>
                </li>
                <li>
                    Exchange Code: <b>654</b>
                </li>
                <li>
                    Subscriber Code: <b>3210</b>
                </li>
            </ul>
        </li>
    </ul>
    <p>
        <a href='{{ url('business') }}' class='btn btn-info btn-lg' role='button'>
            Back to Business
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
