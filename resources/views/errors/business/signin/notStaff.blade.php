@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='lead text-danger'>
        That user is not a staff member or does not exist.
    </p>
    <p class='text-info'>
        That user is not a staff member of {{ $business->name }} or does not exist.
    </p>
    <p>
        <a href='{{ url('business') }}' class='btn btn-info btn-lg' role='button'>
            Back to Business
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
