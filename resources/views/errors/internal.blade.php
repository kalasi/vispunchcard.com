@extends('layouts.default')
@section('content')
<?php
$uri = $_SERVER['REQUEST_URI'];
?>
<div class='wrapper'>
    <p class='text-danger'>
        The server is currently experiencing issues.  Try reloading the page.
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
