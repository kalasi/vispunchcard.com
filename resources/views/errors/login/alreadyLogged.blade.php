@extends('layouts.default')
@section('content')
<div class='row wrapper'>
    <p class='lead text-danger'>
        There was an issue with logging you in to your account.
    </p>
    <p class='text-info'>
        You are already logged in to your account!
    </p>
    <p>
        <a href='{{ url('logout') }}' class='btn btn-info btn-lg' role='button'>
            Logout
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
