@extends('layouts.default')
@section('content')
<div class='row wrapper'>
    <p class='lead text-danger'>
        There was an issue with logging you in to your business.
    </p>
    <p class='text-info'>
        The following are required:
    </p>
    <ul>
        <li>
            Phone Number (987) 654-3210 where <b>N</b> is any number 2 and above and <b>X</b> is any number:
            <ul>
                <li>
                    Area Code: <b>(987)</b> as <b>N</b>XX
                </li>
                <li>
                    Exchange Code: <b>654</b>
                </li>
                <li>
                    Subscriber Code: <b>3210</b>
                </li>
            </ul>
        </li>
        <li>
            Business Name
        </li>
        <li>
            4-Digit PIN Number
        </li>
    </ul>
    <p class='text-info'>
        It seems the input was unreadable and was not in the following format: (NXX) NXX-XXXX, where N is a number greater than 2 and X is any number.
    </p>
    <p>
        <a href='{{ url('login/business') }}' class='btn btn-info btn-lg' role='button'>
            Login
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
