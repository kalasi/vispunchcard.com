@extends('layouts.default')
@section('content')
<div class='row wrapper'>
    <p class='lead text-danger'>
        There was an issue with logging you in to your business.
    </p>
    <p class='text-info'>
        It seems as though that phone number isn't signed up with VisPunchcard.  You need to sign up first!
    </p>
    <p>
        <a href='{{ url('signup') }}' class='btn btn-info btn-lg' role='button'>
            Sign Up
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
