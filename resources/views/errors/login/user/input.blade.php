@extends('layouts.default')
@section('content')
<div class='row wrapper'>
    <p class='lead text-danger'>
        There was an issue with logging you in to your account.
    </p>
    <p class='text-info'>
        It seems the input was unreadable and was not in the following format: (NXX) NXX-XXXX, where N is a number greater than 2 and X is any number.
    </p>
    <p>
        <a href='{{ url('login') }}' class='btn btn-info btn-lg' role='button'>
            Login
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
