@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='lead text-danger'>
        There was an issue while logging in.
    </p>
    <p class='text-info'>
        You're already logged in!  You can't login if you're already logged in.
    </p>
    <p>
        <a href='{{ url('logout') }}' class='btn btn-primary btn-lg' role='button'>
            Logout
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-primary btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
