@extends('layouts.default')
@section('content')
<div class='row wrapper'>
    <p class='lead text-danger'>
        There was an issue with sending your message.
    </p>
    <ul class='text-primary'>
@foreach($errors->all() as $error)
        <li>
            {{ $error }}
        </li>
@endforeach
    </ul>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Try Again
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
