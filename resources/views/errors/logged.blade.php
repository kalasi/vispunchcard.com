@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='text-lead text-danger'>
        You are logged in and can not perform that action when you are logged in.
    </p>
    <p class='text-info'>
        You can't perform that issue while logged in.  You may logout and try again by clicking the button below.
    </p>
    <p>
        <a href='{{ url('logout') }}' class='btn btn-info btn-lg' role='button'>
            Logout
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
