@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='text-warning'>
        You're already logged out!
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
