@extends('layouts.default')
@section('content')
<?php
$uri=$_SERVER['REQUEST_URI'];
?>
<div class='wrapper'>
    <p class='text-danger'>
        The page requested at <b>{{ $uri }}</b> wasn't found!
    </p>
    <p class='text-info'>
        The page may have never existed, might have been taken down, or moved.  If this page existed, please message us <a href='{{ url('contact/page=' . $uri) }}'>at our contact form</a>.
    </p>
    <p>
        <a href='{{ url('contact/page=' . $uri) }}' class='btn btn-info btn-lg' role='button'>
            Message Us in Case of Error
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
