@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='text-danger'>
        You don't have access to the business you're trying to view.
    </p>
    <p class='text-info'>
        You aren't the owner of the business you're trying to view.  Even if you're an employee of the business, only the <b>owner</b> or an <b>administrator</b> or the business may manage it.
    </p>
    <p>
        <a href='{{ url('manage') }}' class='btn btn-info btn-lg' role='button'>
            Management Center
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
