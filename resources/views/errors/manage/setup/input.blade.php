@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='lead text-danger'>
        There was an issue while setting up your business.
    </p>
    <p class='text-info'>
        It seems you didn't fill out all of the fields correctly.  The following are required:
    </p>
    <ul class='text-info'>
        <li>
            Business Name
        </li>
        <li>
            Location
            <ul>
                <li>
                    City
                </li>
                <li>
                    State
                </li>
                <li>
                    Country
                </li>
            </ul>
        </li>
        <li>
            Punches Required per Punchcard
        </li>
        <li>
            4-Digit PIN Number
        </li>
    </ul>
    <p>
        <a href='{{ url('manage/setup') }}' class='btn btn-primary' role='button'>
            Try Again
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
