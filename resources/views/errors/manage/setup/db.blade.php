@extends('layouts.default')
@section('content')
<div class='row wrapper'>
    <p class='lead text-danger'>
        There was an issue while setting up your business.
    </p>
    <p class='text-info'>
        It seems that the database did something in error.  We don't know what, you probably shouldn't ever see this error.
    </p>
    <p>
        <a href='{{ url('manage/setup') }}' class='btn btn-primary' role='button'>
            Try Again
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
