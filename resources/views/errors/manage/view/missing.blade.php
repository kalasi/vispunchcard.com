@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='text-danger'>
        The business that you tried to view doesn't exist!
    </p>
    <p class='text-info'>
        The business with an ID of <b>{{ $id }}</b> doesn't seem to exist.
    </p>
    <p>
        <a href='{{ url('manage') }}' class='btn btn-info btn-lg' role='button'>
            Management Center
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-info btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
