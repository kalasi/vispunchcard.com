@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='lead text-success'>
        The {{ strtolower($role->name) }} could not be found.
    </p>
    <p class='text-info'>
        The {{ strtolower($role->name) }} with an ID of <b>{{ $id }}</b> could not be found as an {{ strtolower($role->name) }} of yours.
    </p>
    <p>
        <a href='{{ url('manage/' . $businessID) }}' class='btn btn-info btn-lg' role='button'>
            Back to Management
        </a>
    </p>
</div>
@stop
