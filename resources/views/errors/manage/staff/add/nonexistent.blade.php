@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='lead text-success'>
        The user with a phone number of {{ $phone }} has not signed up.
    </p>
    <p class='text-info'>
        Before they can be added as an employee, they need to sign up to VisPunchcard.
    </p>
    <p>
        <a href='{{ url('manage/' . $businessID) }}' class='btn btn-info btn-lg' role='button'>
            Back to Management
        </a>
    </p>
    <p>
        <a href='{{ url('logout/to=signup') }}' class='btn btn-info btn-lg' role='button'>
            Logout and Signup
        </a>
    </p>
</div>
@stop
