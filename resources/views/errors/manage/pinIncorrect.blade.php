@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='lead text-danger'>
        There was an issue with your 4-Digit PIN.
    </p>
    <p class='text-info'>
        The PIN that you entered was incorrect for the business <b>{{ $business->name }}</b>.
    </p>
    <p>
        <a href='{{ url('manage/' . $business->id) }}' class='btn btn-primary btn-lg' role='button'>
            Try Again
        </a>
    </p>
    <p>
        <a href='{{ url('manage') }}' class='btn btn-primary btn-lg' role='button'>
            Management Center
        </a>
    </p>
    <p>
        <a href='{{ url() }}' class='btn btn-primary btn-lg' role='button'>
            Back to Homepage
        </a>
    </p>
</div>
@stop
