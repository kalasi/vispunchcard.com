@extends('layouts.default')
@section('content')
<div class='wrapper row'>
    <div class='col-xs-12 col-sm-6'>
        <div class='well well-dark'>
            <h2>
                JSON Output
            </h2>
            <details>
                <summary>
                    Show/Hide Output
                </summary>
                <p>
                    {{ $json }}
                </p>
            </details>
        </div>
    </div>
    <div class='col-xs-12 col-sm-6'>
        <div class='well well-dark'>
            <h2>
                CSV Output
            </h2>
            <details>
                <summary>
                    Show/Hide Output
                </summary>
                <p>
                    {!! $csv !!}
                </p>
            </details>
        </div>
    </div>
    <div class='well well-dark col-xs-12'>
        <h2 class='text-center'>
            Plain Text
        </h2>
        <details>
            <summary>
                Show/Hide Output
            </summary>
            <p>
                {!! $plain !!}
            </p>
        </details>
    </div>
</div>
@stop
