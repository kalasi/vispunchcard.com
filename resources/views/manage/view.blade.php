@extends('layouts.default')
@section('content')
<div class='wrapper row'>
    <blockquote>
        <p>
            This is the Management center of your business.  To use the Checkout system, you need to logout and login as a business.
        </p>
        <a href='{{ url('manage/' . $business->id . '/switch') }}' class='btn btn-primary' role='button'>
            Switch to {{ $business->name }}
        </a>
    </blockquote>
    <div class='col-xs-12 col-md-6'>
        <h2>
            Statistics
        </h2>
        <dl>
            <dt>
                Total Punches
            </dt>
            <dd>
                {{ $punches }}
            </dd>
            <dt>
                Total Punchcards Used
            </dt>
            <dd>
                {{ $used }}
            </dd>
            <dt>
                Transactions
            </dt>
            <dd>
                {{ $transactions }}
            </dd>
            <dt>
                Customers
            </dt>
            <dd>
                {{ $customers }}
            </dd>
            <dt>
                eMail Addresses
            </dt>
            <dd>
                {{ $addresses }} <a href='{{ url('manage/' . $business->id . '/addresses') }}' title='View Addresses'><i class='fa fa-search'></i> View</a>
            </dd>
        </dl>
    </div>
    <div class='col-xs-12 col-md-6'>
        <h3>
            Administrators
        </h3>
        <ul class='list-inline'>
            <li>
                <a href='{{ url('manage/' . $business->id . '/admin/') }}' title='Manage Administrators'>
                    <i class='fa fa-users'></i> Manage
                </a>
            </li>
        </ul>
@if(empty($admins))
        <p class='text-info'>
            There are no administrators for this business.
        </p>
@endif
@foreach($admins as $admin)
        <div class='well well-sm well-dark'>
            <p>
                {{ $admin->fname }}
            </p>
            <p>
                ({{ $admin->area }}) {{ $admin->exchange }}-{{ $admin->sub }}
            </p>
@if($isOwner)
            <ul class='list-inline'>
                <li>
                    <a href='{{ url('manage/' . $business->id . '/admin/' . $admin->id . '/remove') }}' title='Remove Employee'>
                        <i class='fa fa-minus'></i> Remove
                    </a>
                </li>
            </ul>
@endif
        </div>
@endforeach
        <h3>
            Employees
        </h3>
        <ul class='list-inline'>
            <li>
                <a href='{{ url('manage/' . $business->id . '/employee/') }}' title='Manage Employees'>
                    <i class='fa fa-users'></i> Manage
                </a>
            </li>
        </ul>
@if(empty($employees))
        <p class='text-info'>
            There are no employees for this business.
        </p>
@endif
@foreach($employees as $employee)
        <div class='well well-sm well-dark'>
            <p>
                {{ $employee->fname }}
            </p>
            <p>
                ({{ $employee->area }}) {{ $employee->exchange }}-{{ $employee->sub }}
            </p>
            <ul class='list-inline'>
                <li>
                    <a href='{{ url('manage/' . $business->id . '/employee/' . $employee->id . '/remove') }}' title='Remove Employee'>
                        <i class='fa fa-minus'></i> Remove
                    </a>
                </li>
            </ul>
        </div>
@endforeach
    </div>
</div>
@stop
