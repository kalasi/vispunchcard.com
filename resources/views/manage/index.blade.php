@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <div class='row'>
        <div class='col-xs-12 col-sm-8'>
            <h2 class='text-primary'>
                Manage Business
            </h2>
@if(!empty($businesses) || !empty($employed))
	@if(!empty($businesses))
            <h3 class='text-info'>
                Owner or Administrator
            </h3>
		@foreach($businesses as $business)
            <div class='well well-sm well-dark'>
                <h4>
                    {{ $business['name'] }}
                </h4>
                <p>
                    {{ $business['city'] }}, {{ $business['municipality'] }}
                </p>
                <ul class='list-inline'>
                    <li>
                        <a href='{{ url('manage/'.$business['id']) }}' title="View {{ $business['name'] }}">
                            <i class='fa fa-search'></i> View
                        </a>
                    </li>
                </ul>
            </div>
		@endforeach
	@endif
	@if(!empty($employed))
            <h3 class='text-info'>
                Employed By
            </h3>
		@foreach($employed as $employ)
            <div class='well well-sm well-dark'>
                <h4>
                    {{ $employ['name'] }}
                </h4>
                <p>
                    {{ $employ['city'] }}, {{ $employ['municipality'] }}
                </p>
                <ul class='list-inline'>
                    <li>
                        <a href='{{ Utilities::URLBusiness($employ['id']) }}' title='Checkout'>
                            <span class='glyphicon glyphicon-shopping-cart'></span> Checkout
                        </a>
                    </li>
                </ul>
            </div>
		@endforeach
	@endif
@else
            <p class='text-danger'>
                It looks like you aren't the owner, administrator, or employee of any businesses!
            </p>
@endif
        </div>
        <div class='col-xs-12 col-sm-4'>
            <h2 class='text-info'>
                Setup New Business
            </h2>
            <p>
                <span class='text-rt'>Have a business that uses punchcards that you would like to turn online or would like to use punchcards?</span>  <span class='text-success'>We'll help you get setup.</span>
            </p>
            <p>
                <a href='{{ url('manage/setup') }}' class='btn btn-primary btn-lg' role='button'>
                    Get Started
                </a>
            </p>
        </div>
    </div>
</div>
@stop
