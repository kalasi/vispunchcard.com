@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <ul class='list-inline'>
        <li>
            <a href='{{ url('manage/' . $business->id . '/' . $role->name_trim . '/add') }}' title='Add {{ $role->name }}'>
                <i class='fa fa-plus'></i> Add {{ $role->name }}
            </a>
        </li>
    </ul>
@if(empty($staffs))
    <p class='text-warning'>
        Looks like you don't have any {{ strtolower($role->names) }} to manage!
    </p>
@else
    @foreach($staffs as $staff)
        @if(!empty($staff))
    <div class='well well-sm well-dark'>
        <p>
            {{ $staff->fname }}
        </p>
        <p>
            ({{ $staff->area }}) {{ $staff->exchange }}-{{ $staff->sub }}
        </p>
        <ul class='list-inline'>
            <li>
                <a href='{{ url('manage/' . $business->id . '/' . $role->name_trim . '/' . $staff->id . '/' . 'remove') }}' title='Remove {{ $staff->fname }}'>
                    <i class='fa fa-minus'></i>Remove
                </a>
            </li>
        </ul>
    </div>
        @endif
    @endforeach
@endif
</div>
@stop
