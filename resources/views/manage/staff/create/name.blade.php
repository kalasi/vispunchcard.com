@extends('layouts.form')
@section('content')
<div class='row wrapper'>
    <form class='form-horizontal' action='manage/{{ $business->id }}/{{ $role->name_trim }}/create' method='post' role='form'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <input type='hidden' name='phone' value='{{ $phone }}' />
        <input type='hidden' name='role' value='{{ Crypt::encrypt($role) }}' />
        <h1>
            VisPunchcard
        </h1>
        <h2>
            Create Account for {{ $role->name }}
        </h2>
        <div>
            <em rt-bind='name' rt-required='1' rt-enter='1'></em>
            <input type='tel' name='name' />
            <span>
                {{$role->name}} Name
            </span>
            <p>
                Your name will only be used on checkouts.
            </p>
            <p>
                You did not fill out a name!
            </p>
        </div>
    </form>
</div>
<script>
    var Form=new Form();
    Form.setup();
</script>
@stop
