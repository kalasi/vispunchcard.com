@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='lead text-success'>
        The person with a phone number of <b>({{ $area }}) {{ $exchange }}-{{ $sub }}</b> has been added as an {{ strtolower($role->name) }}.
    </p>
    <p class='text-info'>
        When they first attempt to perform an action related to <b>{{ $business->name }}</b>, they will be prompted for your business PIN number.
    </p>
@if($role->name_trim == "admin")
    <p class='text-info'>
        {{ $role->names }} may manage almost every part of your business. {{ $role->names }} can not add or remove other {{ $role->names }}.  It is highly recommended to give your new {{ $role->name }} your business PIN number.
    </p>
@endif
    <p>
        <a href='{{ url('manage/' . $business->id) }}' class='btn btn-info btn-lg' role='button'>
            Back to Management
        </a>
    </p>
</div>
@stop
