@extends('layouts.form')
@section('content')
<div class='row wrapper'>
    <form class='form-horizontal' action='' method='post' role='form'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <em rt-header='Add {{ $role->name }}'></em>
        <div>
            <em rt-bind='phone' rt-required='1'></em>
            <input type='tel' name='phone' />
            <span>
                {{ $role->name }} Phone Number
            </span>
            <p>
                We'll automatically format your phone number.
            </p>
            <p>
                That is not a proper US phone number.
            </p>
        </div>
    </form>
</div>
<script>
    var Form = new Form();
    Form.setup();
</script>
@stop
