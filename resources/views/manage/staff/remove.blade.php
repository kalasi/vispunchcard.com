@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='lead text-success'>
        The administrator <b>{{ $fname }}</b> has been removed.
    </p>
    <p>
        <a href='{{ url('manage/' . $businessID) }}' class='btn btn-info btn-lg' role='button'>
            Back to Management
        </a>
    </p>
</div>
@stop
