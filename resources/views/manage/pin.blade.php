@extends('layouts.form')
@section('content')
<div class='row wrapper'>
    <form class='form-horizontal' action='' method='post' role='form'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <em rt-header='Enter Business PIN'></em>
        <div>
            <em rt-bind='pin' rt-required='1'></em>
            <input type='number' name='pin' />
            <span>
                4-Digit PIN
            </span>
            <p>
                This is the PIN number to your business.
            </p>
            <p>
                You did not enter a positive 4-digit number!
            </p>
        </div>
    </form>
</div>
<script>
    var Form=new Form();
    Form.setup();
</script>
@stop
