@extends('layouts.form')
@section('content')
<div class='row wrapper'>
    <form class='form-horizontal' action='' method='post' role='form'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <em rt-header='Setup Your Business'></em>
        <div>
            <em rt-bind='name' rt-required='1' rt-enter='1' rt-placeholder='Enter Business Name Here'></em>
            <input type='text' name='name' />
            <span>
                Business Name
            </span>
            <p>
                The business name will be used throughout the website and used on checkouts.
            </p>
            <p>
            </p>
        </div>
        <div>
            <em rt-bind='name' rt-required='1' rt-enter='1' rt-placeholder='ex: Seattle'></em>
            <input type='text' name='city' />
            <span>
                Location: City Name
            </span>
            <p>
                The location will be used on checkout.
            </p>
            <p>
            </p>
        </div>
        <div>
            <em rt-bind='name' rt-required='1' rt-enter='1' rt-placeholder='ex: Washington'></em>
            <input type='text' name='municipality' />
            <span>
                Location: State Name
            </span>
            <p>
                The location will be used on checkout.
            </p>
            <p>
            </p>
        </div>
        <div>
            <em rt-bind='integer' rt-required='1' rt-enter='1' rt-placeholder='ex: 12'></em>
            <input type='text' name='punches_required' />
            <span>
                Punches Required Per Punchcard
            </span>
            <p>
                Press Enter when done.  This is the number of punches required for a complete punchcard.
            </p>
            <p>
                The number is a negative number.  Not really possible, unfortunately.
            </p>
        </div>
        <div>
            <em rt-bind='pin' rt-required='1'></em>
            <input type='number' name='pin' />
            <span>
                4-Digit PIN Number
            </span>
            <p>
                This will be your secure PIN number to manage your business.
            </p>
            <p>
                The number is not a positive 4 digit PIN number.
            </p>
        </div>
    </form>
</div>
<script>
    var Form=new Form();
    Form.setup();
</script>
@stop
