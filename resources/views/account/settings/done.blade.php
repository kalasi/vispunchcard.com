@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='text-lead text-success'>
        Your account settings have been successfully changed.  The following have been changed:
    </p>
    <ul class='text-info'>
@foreach($changedSettings as $field)
        <li>
            {{ $field->name }}: {{ $field->value}}
        </li>
@endforeach
    </ul>
    <p>
        <a href='{{ url('account') }}' class='btn btn-primary btn-lg' role='button'>
            My Account
        </a>
    </p>
</div>
@stop
