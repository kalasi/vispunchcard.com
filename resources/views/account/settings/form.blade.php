@extends('layouts.default')
@section('content')
<div class='wrapper'>
    <p class='text-info'>
        Only change the settings you want to change.  The rest won't be changed if they aren't filled out.
    </p>
    <form action='' method='post' role='form' class='form-horizontal'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <div class='form-group'>
            <label for='{{ $settings->fname->name }}' class='col-sm-2 control-label'>
                {{ $settings->fname->label }}
            </label>
            <div class='col-sm-10'>
                <input type='{{ $settings->fname->type }}' class='form-control' id='{{ $settings->fname->name }}' name='{{ $settings->fname->name }}' placeholder='{{ $settings->fname->placeholder }}' />
            </div>
        </div>
        <button class='btn btn-info' type='submit'>
            Change Settings
        </button>
    </form>
</div>
@stop
