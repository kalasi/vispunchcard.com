@extends('layouts.default')
@section('content')
<div class='wrapper'>
@foreach($punchcards as $punchcard)
    <div class='well well-sm punchcard'>
        <h4>
            {{ $punchcard->name }}
        </h4>
        <p>
            {{ $punchcard->city }}, {{ $punchcard->municipality }}
        </p>
	@if($punchcard->full > 0)
        <b class='text-success'>
            {{ $punchcard->full }} punchcards full
        </b>
        <br />
	@endif
	@for($x = 1; $x <= $punchcard->full; $x++)
		@for($y=1; $y <= 12; $y++)
        <span class='punch'></span>
		@endfor
        <span class='text-success glyphicon glyphicon-ok'></span><br />
	@endfor
	@for($x = 1; $x <= $punchcard->current; $x++)
        <span class='punch'></span>
	@endfor
	@for($x = $punchcard->current; $x < $punchcard->punches_required; $x++)
        <span class='punch-empty'></span>
	@endfor
	@if(!empty($punchcard->transactions))
        <details>
            <summary>
                Click here to show/hide your transaction history
            </summary>
            <p>
                <table class='table'>
                    <thead>
                        <tr>
                            <th>
                                ID
                            </th>
                            <th>
                                Punches Received
                            </th>
                            <th>
                                Punchcards Used
                            </th>
                            <th>
                                Time
                            </th>
                        </tr>
                    </thead>
                    <tbody>
<?php $x = $punchcard->transactionAmount; ?>
		@foreach($punchcard->transactions as $transaction)
                        <tr{{ $transaction->used > 0 ? " class='background-warning'" : "" }}>
                            <td>
                                {{ $x }}
                            </td>
                            <td>
                                {{ $transaction->punches }}
                            </td>
                            <td>
                                {{ $transaction->used }}
                            </td>
                            <td>
                                {{ Time::long($transaction->timestamp) }}
                            </td>
                        </tr>
<?php $x--; ?>
		@endforeach
                    </tbody>
                </table>
            </p>
        </details>
	@endif
    </div>
@endforeach
@if(empty($punchcards))
    <p class='text-warning'>
        You don't seem to have any punchcards!
    </p>
@endif
</div>
@stop
