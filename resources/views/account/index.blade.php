@extends('layouts.default')
@section('content')
<div class='wrapper'>
    Hello, {{ User::$fname }}.
    <div class='row'>
        <div class='col-xs-12 col-md-8'>
@if(!empty($businesses))
            <h2 class='text-center'>
                Owner Of
            </h2>
@endif
@foreach($businesses as $business)
            <div class='well well-sm well-dark'>
                <p>
                    {{ $business->name }}
                </p>
                <p>
                    {{ $business->city }}, {{ $business->municipality }}
                </p>
                <ul class='list-inline'>
                    <li>
                        <a href='{{ url('manage/' . $business->id) }}' title="View {{ $business->name }}">
                            <i class='fa fa-search'></i> View
                        </a>
                    </li>
                </ul>
            </div>
@endforeach
@if(!empty($employedBy))
            <h2 class='text-info'>
                Employed By
            </h2>
@endif
@foreach($employedBy as $employment)
            <div class='well well-sm well-dark'>
                <p>
                    {{ $employment->name }}
                </p>
                <p>
                    {{ $employment->city }}, {{ $employment->municipality }}
                </p>
                <ul class='list-inline'>
                    <li>
                        <a href='{{ Utilities::URLBusiness($employment->id) }}' title="Edit {{ $employment->name }}">
                            <i class='fa fa-shopping-cart'></i>Checkout
                        </a>
                    </li>
                </ul>
            </div>
@endforeach
            <h2 class='text-center'>
                Punchcards
            </h2>
@foreach($punchcards as $punchcard)
            <div class='well well-sm punchcard'>
                <h4>
                    {{ $punchcard['name'] }}
                </h4>
                <p>
                    {{ $punchcard['city'] }}, {{ $punchcard['municipality'] }}
                </p>
	@if($punchcard['full'] > 0)
                <b class='text-success'>
                    {{ $punchcard['full'] }} punchcards full
                </b>
                <br />
	@endif
	@for($x = 1; $x <= $punchcard['current']; $x++)
                <span class='punch'></span>
	@endfor
	@for($x = $punchcard['current']; $x < $punchcard['punches_required']; $x++)
                <span class='punch-empty'></span>
	@endfor
            </div>
@endforeach
@if(empty($punchcards))
            <p class='text-warning'>
                You don't seem to have any punchcards!
            </p>
@endif
        </div>
        <div class='col-xs-12 col-md-4'>
@if(!empty($businesses))
            <h2 class='text-center'>
                Manage Businesses
            </h2>
            <p>
                To view, manage, and change your current businesses you can click <a href='{{ url('manage') }}' title='Manage Current Businesses'>here</a>
            </p>
            <p>
                <a href='{{ url('manage') }}' class='btn btn-primary btn-lg' role='button'>
                    Manage Businesses
                </a>
            </p>
@endif
            <h2 class='text-center'>
                Setup Your Business
            </h2>
            <p>
                <span class='text-rt'>Have a business that uses punchcards that you would like to turn online or would like to use punchcards?</span><span class='text-success'> We'll help you get setup.</span>
            </p>
            <p>
                <a href='{{ url('manage/setup') }}' class='btn btn-primary btn-lg' role='button'>
                    Get Started
                </a>
            </p>
        </div>
    </div>
</div>
@stop
