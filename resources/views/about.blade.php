@extends('layouts.default')
@section('content')
<div class='wrapper-green wrapper-large'>
    <p class='text-center lead'>
        VisPunchcard is an electronic, simple-to-use, and efficient method of managing punchcards.
    </p>
</div>
<div class='wrapper'>
    <p class='text-center text-info'>
        Useable on any device, VisPunchcard is a 3-step process available for your business to use on your phone, tablet, or computer.
    </p>
    <div class='row text-center'>
        <div class='col-sm-4'>
            <i class='fa fa-mobile fa-5x'></i>
            <h2 class='text-info no-margin'>
                Phones
            </h2>
        </div>
        <div class='col-sm-4'>
            <i class='fa fa-tablet fa-5x'></i>
            <h2 class='text-info no-margin'>
                Tablet
            </h2>
        </div>
        <div class='col-sm-4'>
            <i class='fa fa-laptop fa-5x'></i>
            <h2 class='text-info no-margin'>
                Computers
            </h2>
        </div>
    </div>
    <p>
        As an owner of a business, you have access to graphs detailing employees' performance, number of punchcard transactions made, number of customers, and more.
    </p>
    <p>
        As a customer, you can view your punchcards online by logging in.  Don't worry, companies don't have access to any of your information.  Companies can never see your phone number and can only see your name on checkout.  Your information is secure and encrypted for security.
    </p>
    <div>
        <details>
            <summary>
                Example of Encrypted Data
            </summary>
            <p class='word-break'>
                {{ $hash }}
            </p>
        </details>
    </div>
    <br />
    <p>
        Most importantly, VisPunchcard is free for everyone to use.  There are no costs associated with VisPunchcard.  Businesses of any size can use VisPunchcard.
    </p>
</div>
@stop
