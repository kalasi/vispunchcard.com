<?php
namespace App\Vis\Punchcards;

use App\RT\Core\Entity;

/**
 * Class Punchcard
 * @package App\Vis\Punchcards
 */
class Punchcard extends Entity
{
    /**
     * The table this model represents.
     *
     * @var string
     */
    protected $table = 'punchcards';

    /**
     * Whether or not records are soft deleted.
     *
     * @var bool
     */
    protected $softDelete = false;
}
