<?php
namespace App\Vis\Punchcards;

use App\RT\Core\EloquentRepository;

/**
 * Class PunchcardRepository
 * @package App\Vis\Punchcards
 */
class PunchcardRepository extends EloquentRepository
{
    /**
     * @param Punchcard $model
     */
    public function __construct(Punchcard $model)
    {
        $this->model = $model;
    }
}
