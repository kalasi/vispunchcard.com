<?php
namespace App\Vis\Admins;

use App\RT\Core\Entity;

/**
 * Class Admin
 * @package App\Vis\Admins
 */
class Admin extends Entity
{
    /**
     * The table this model represents.
     *
     * @var string
     */
    protected $table = 'admins';

    /**
     * Whether or not records are soft deleted.
     *
     * @var bool
     */
    protected $softDelete = false;
}
