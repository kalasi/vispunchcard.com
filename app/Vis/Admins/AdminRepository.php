<?php
namespace App\Vis\Admins;

use App\RT\Core\EloquentRepository;

/**
 * Class AdminRepository
 * @package App\Vis\Admins
 */
class AdminRepository extends EloquentRepository
{
    /**
     * @param Admin $model
     */
    public function __construct(Admin $model)
    {
        $this->model = $model;
    }
}
