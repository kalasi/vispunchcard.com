<?php
namespace App\Vis\Employees;

use App\RT\Core\EloquentRepository;

/**
 * Class EmployeeRepository
 * @package App\Vis\Employees
 */
class EmployeeRepository extends EloquentRepository
{
    /**
     * @param Employee $model
     */
    public function __construct(Employee $model)
    {
        $this->model = $model;
    }
}
