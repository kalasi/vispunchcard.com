<?php
namespace App\Vis\Employees;

use App\RT\Core\Entity;

/**
 * Class Employee
 * @package App\Vis\Employees
 */
class Employee extends Entity
{
    /**
     * The table this model represents.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * Whether or not records are soft deleted.
     *
     * @var bool
     */
    protected $softDelete = false;
}
