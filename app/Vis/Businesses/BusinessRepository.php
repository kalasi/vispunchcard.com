<?php
namespace App\Vis\Businesses;

use App\RT\Core\EloquentRepository;

/**
 * Class BusinessRepository
 * @package App\Vis\Businesses
 */
class BusinessRepository extends EloquentRepository
{
    /**
     * @param Business $model
     */
    public function __construct(Business $model)
    {
        $this->model = $model;
    }
}
