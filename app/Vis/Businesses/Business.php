<?php
namespace App\Vis\Businesses;

use App\RT\Core\Entity;

/**
 * Class Business
 * @package App\Vis\Businesses
 */
class Business extends Entity
{
    /**
     * The table this model represents.
     *
     * @var string
     */
    protected $table = 'businesses';

    /**
     * Whether or not records are soft deleted.
     *
     * @var bool
     */
    protected $softDelete = false;
}
