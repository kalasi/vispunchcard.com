<?php
namespace App\Vis\Messages;

use App\RT\Core\EloquentRepository;

/**
 * Class MessageRepository
 * @package App\Vis\Messages
 */
class MessageRepository extends EloquentRepository
{
    /**
     * @param Message $model
     */
    public function __construct(Message $model)
    {
        $this->model = $model;
    }
}
