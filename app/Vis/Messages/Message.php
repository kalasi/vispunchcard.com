<?php
namespace App\Vis\Messages;

use App\RT\Core\Entity;

/**
 * Class Message
 * @package App\Vis\Messages
 */
class Message extends Entity
{
    /**
     * The table this model represents.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
     * Whether or not records are soft deleted.
     *
     * @var bool
     */
    protected $softDelete = false;
}
