<?php
namespace App\Vis\Transactions;

use App\RT\Core\Entity;

/**
 * Class Transaction
 * @package App\Vis\Transactions
 */
class Transaction extends Entity
{
    /**
     * The table this model represents.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
     * Whether or not records are soft deleted.
     *
     * @var bool
     */
    protected $softDelete = false;
}
