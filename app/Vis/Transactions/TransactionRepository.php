<?php
namespace App\Vis\Transactions;

use App\RT\Core\EloquentRepository;

/**
 * Class TransactionRepository
 * @package App\Vis\Transactions
 */
class TransactionRepository extends EloquentRepository
{
    /**
     * @param Transaction $model
     */
    public function __construct(Transaction $model)
    {
        $this->model = $model;
    }
}
