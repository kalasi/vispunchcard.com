<?php
namespace App\Vis\Customers;

use App\RT\Core\EloquentRepository;

/**
 * Class CustomerRepository
 * @package App\Vis\Customers
 */
class CustomerRepository extends EloquentRepository
{
    /**
     * @param Customer $model
     */
    public function __construct(Customer $model)
    {
        $this->model = $model;
    }
}
