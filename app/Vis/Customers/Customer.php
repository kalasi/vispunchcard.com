<?php
namespace App\Vis\Customers;

use App\RT\Core\Entity;

/**
 * Class Customer
 * @package App\Vis\Customers
 */
class Customer extends Entity
{
    /**
     * The table this model represents.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
     * Whether or not records are soft deleted.
     *
     * @var bool
     */
    protected $softDelete = false;
}
