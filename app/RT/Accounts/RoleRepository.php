<?php
namespace App\RT\Accounts;

use App\RT\Core\EloquentRepository;

class RoleRepository extends EloquentRepository
{
    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    public function getRoleList()
    {
        return $this->model
            ->lists('id','name');
    }

    public function getByName($name)
    {
        return $this->model
            ->where('name',$name)
            ->first();
    }

    public function getById($id)
    {
        return $this->model
            ->where('id',$id)
            ->first();
    }
}
