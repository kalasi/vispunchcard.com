<?php
namespace App\RT\Accounts;

use App\RT\Core\Entity;

class Role extends Entity
{
    protected $table = 'user_groups';
    protected $fillable = ['name'];
    protected $validationRules = [
        'name' => 'required'
    ];

    public function users()
    {
        $this->belongsToMany('RT\Accounts\User');
    }
}
