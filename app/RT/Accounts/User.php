<?php
namespace App\RT\Accounts;

use App\RT\Core\Entity;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Entity implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    const STATE_ACTIVE=1;
    const STATE_BLOCKED=2;
    protected $table = 'users';
    protected $fillable = ['role','fname','lname','email','password'];
    protected $softDelete = false;
    public $presenter = 'RT\Accounts\UserPresenter';
    protected $validationRules = [
        'fname'    => 'required',
        'lname'    => 'required',
        'email'    => 'required',
        'password' => 'required',
    ];
    private $rolesCache;

    public function roles()
    {
        return $this->belongsToMany('RT\Accounts\Role');
    }

    public function getRoles()
    {
        if (!isset($this->rolesCache)) {
            $this->rolesCache = $this->roles;
        }

        return $this->rolesCache;
    }

    public function hasRole($roleName)
    {
        return $this->hasRoles($roleName);
    }

    public function hasRoles($roleNames = [])
    {
        $roleList = \App::make('RT\Accounts\RoleRepository')
            ->getRoleList();
        foreach ((array) $roleNames as $allowedRole) {
            if (!in_array($allowedRole, $roleList)) {
                throw new InvalidRoleException("Unidentified role: " . $allowedRole);
            }

            if (!$this->roleCollectionHasRole($allowedRole)) {
                return false;
            }
        }

        return true;
    }

    private function roleCollectionHasRole($allowedRole)
    {
        $roles = $this->getRoles();
        if (!$roles) {
            return false;
        }

        foreach ($roles as $role) {
            if (strtolower($role->name) == strtolower($allowedRole)) {
                return true;
            }
        }

        return false;
    }

    /**
     * UserInterface
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * RemindableInterface
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($newValue)
    {
        $this->remember_token = $newValue;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        // TODO: Implement getEmailForPasswordReset() method.
    }
}
