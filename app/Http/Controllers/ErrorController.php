<?php
namespace App\Http\Controllers;

/**
 * Class ErrorController
 * @package App\Http\Controllers
 */
class ErrorController extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function logged()
    {
        $page = \View::make('errors.logged');
        $page->with('nav', 'Error');
        $page->with('title', 'You Are Logged In');

        return $page;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function notBusiness()
    {
        $page = \View::make('errors.notUser');

        $page->with('nav', 'Error');
        $page->with('title', 'Not Signed In As User');

        return $page;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function notUser()
    {
        $page = \View::make('errors.notUser');

        $page->with('nav', 'Error');
        $page->with('title', 'Not Signed In As User');

        return $page;
    }
}
