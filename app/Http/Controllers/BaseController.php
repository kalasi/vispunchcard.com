<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

/**
 * Class BaseController
 * @package App\Http\Controllers
 */
class BaseController extends Controller
{
    /**
     *
     */
    public function __construct()
    {
        \User::login();
    }
}
