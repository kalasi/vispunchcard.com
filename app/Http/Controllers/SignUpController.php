<?php
namespace App\Http\Controllers;

use App\User;

/**
 * Class SignUpController
 * @package App\Http\Controllers
 */
class SignUpController extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (!\User::$logged) {
            $page = \View::make('signup.index');

            $page->with('title', 'Sign Up');
            $page->with('css', 'rt-form');
            $page->with('js', 'rt-form');
        } else {
            $page = \View::make('errors.signup.alreadyLogged');

            $page->with('bc', ['signup' => 'Sign Up']);
            $page->with('title', 'Already Logged In');
        }

        return $page;
    }

    /**
     * @return $this|\Illuminate\View\View
     */
    public function submitted()
    {
        if (!\User::$logged) {
            $fname = \Input::get('fname');

            $data = \Utilities::processPhone(\Input::get('phone'));
            $data['fname'] = $fname;

            $requirements = [
                'area'     => 'required|size:3',
                'exchange' => 'required|size:3',
                'sub'      => 'required|size:4|min:0',
                'fname'    => 'required',
            ];

            $validator = \Validator::make($data,$requirements);

            if ($validator->passes()) {
                $hashedPassword = \Hash::make('pass');

                $user = new User;

                $user->fname=$fname;
                $user->area=$data['area'];
                $user->exchange=$data['exchange'];
                $user->sub=$data['sub'];
                $user->password=$hashedPassword;

                $user->save();

                $credentials = [
                    'area'     => $data['area'],
                    'exchange' => $data['exchange'],
                    'sub'      => $data['sub'],
                    'password' => 'pass',
                ];

                if (\Auth::attempt($credentials, true)) {
                    $cookie = \Cookie::forever('user-type', 'user');

                    return \Redirect::to('account')
                        ->withCookie($cookie);
                }
            }

            $page = \View::make('errors.signup.input');

            $page->with('title', 'Error Signing Up');
            $page->with('titleSub', 'Validation');
        } else {
            $page = \View::make('errors.signup.alreadyLogged');

            $page->with('title', 'Error Signing Up');
            $page->with('titleSub', 'Already Logged In');
        }

        return $page;
    }
}
