<?php
namespace App\Http\Controllers;

/**
 * Class LogoutController
 * @package App\Http\Controllers
 */
class LogoutController extends BaseController
{
    /**
     * @param string $url
     *
     * @return $this
     */
    public function index($url = "")
    {
        \Auth::logout();
        \User::login();

        $cookie = \Cookie::forget('user-type');

        $cookieBI = \Cookie::forget('business-info');

        $cookieSI = \Cookie::forget('signed-in');

        if (!empty($url)) {
            return \Redirect::to($url)
                ->withCookie($cookie);
        }

        return \Redirect::to('logout/done')
            ->withCookie($cookie)
            ->withCookie($cookieBI)
            ->withCookie($cookieSI);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function done()
    {
        $page = \View::make('logout');

        $page->with('nav', 'Logout');
        $page->with('title', "You've Been Logged Out");

        return $page;
    }
}
