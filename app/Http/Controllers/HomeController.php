<?php
namespace App\Http\Controllers;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $page = \View::make('home');

        $page->with('nav','Home');
        $page->with('css','home');
        $page->with('js','home');

        return $page;
    }
}
