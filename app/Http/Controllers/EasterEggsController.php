<?php
namespace App\Http\Controllers;

/**
 * Class EasterEggsController
 * @package App\Http\Controllers
 */
class EasterEggsController extends BaseController
{
    /**
     * @param $name
     *
     * @return \Illuminate\View\View
     */
    public function princess($name)
    {
        $page = \View::make('eastereggs.princess');

        $page->with('title', 'Sorry, ' . $name);
        $page->with('name', $name);

        return $page;
    }
}
