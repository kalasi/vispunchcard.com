<?php
namespace App\Http\Controllers;

/**
 * Class BusinessController
 * @package App\Http\Controllers
 */
class BusinessController extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $cookie = \Business::cookie();

        $business = \Business::info($cookie->id);

        $staff = [];

        $owner = \DB::table('users')
            ->select('fname')
            ->where('id', $business->owner)
            ->first();

        $admins = \DB::table('admins')
            ->select('user')
            ->where('business', $business->id)
            ->get();

        $employees = \DB::table('employees')
            ->select('user')
            ->where('business', $business->id)
            ->get();

        $staff[$owner->fname] = [
            'id'   => $business->owner,
            'name' => $owner->fname,
        ];

        foreach ($admins as $admin) {
            $adminInfo = \DB::table('users')
                ->select('fname')
                ->where('id', $admin->user)
                ->first();

            $staff[$adminInfo->fname] = [
                'id'   => $admin->user,
                'name' => $adminInfo->fname,
            ];
        }
        foreach ($employees as $employee) {
            $employeeInfo = \DB::table('users')
                ->select('fname')
                ->where('id', $employee->user)
                ->first();

            $staff[$employeeInfo->fname] = [
                'id'   => $employee->user,
                'name' => $employeeInfo->fname,
            ];
        }

        $signedIn = \Business::signedInInfo();

        ksort($staff);

        $staff = (object) json_decode(json_encode($staff));

        $page = \View::make('business.index');

        $page->with('bc', ['business' => 'Business']);
        $page->with('nav', 'Business');
        $page->with('title', $business->name);
        $page->with('business', $business);
        $page->with('signedIn', $signedIn);
        $page->with('staffList', $staff);

        return $page;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function signIn($id)
    {
        $business = \Business::info(\Business::cookie()->id);

        if (\Business::isStaff($id)) {
            $userInfo = \DB::table('users')
                ->select('id', 'fname')
                ->where('id', $id)
                ->first();

            $user=\Business::signedIn();

            if ($user) {
                if (\Business::signedInInfo()->id == $id) {
                    return \Redirect::to(\Utilities::URLBusiness($business->id));
                }

                $page = \View::make('errors.business.signedIn');

                $page->with('bc', ['business' => 'Business']);
                $page->with('title', 'Already Signed In');
                $page->with('business', $business);
                $page->with('user', $userInfo);
            } else {
                $page = \View::make('business.signin.form');

                $page->with('title', 'Sign In to ' . $business->name);
                $page->with('css', 'rt-form');
                $page->with('business', $business);
            }
        } else {
            $page = \View::make('errors.business.signin.notStaff');

            $page->with('bc', ['business' => 'Business']);
            $page->with('title', 'Not a Staff Member');
            $page->with('business', $business);
        }

        return $page;
    }

    /**
     * @param $id
     *
     * @return $this|\Illuminate\View\View
     */
    public function signInDone($id)
    {
        $data = \Utilities::processPhone(\Input::get('phone'));
        $requirements = [
            'area'     => 'required|size:3',
            'exchange' => 'required|size:3',
            'sub'      => 'required|size:4|min:0',
        ];

        $validator = \Validator::make($data,$requirements);

        if ($validator->passes()) {
            $user = \DB::table('users')
                ->select('fname')
                ->where('id',$id)
                ->first();

            $correct = \DB::table('users')
                ->select('id')
                ->where('area', $data['area'])
                ->where('exchange', $data['exchange'])
                ->where('sub', $data['sub'])
                ->first();

            if ($correct->id == $id) {
                $cookieData = \Crypt::encrypt(serialize(\Crypt::encrypt($data)));
                $cookie = \Cookie::forever('signed-in', $cookieData);

                return \Redirect::to(
                    \Utilities::URLBusiness(
                        \Business::cookie()->id))
                    ->withCookie($cookie);
            } else {
                $page = \View::make('errors.business.signin.phone');
                $page->with('bc', ['business' => 'Business']);
                $page->with('nav', 'Business');
                $page->with('title', 'Logging Into ' . $user->fname . '\'s Account');
            }
        } else {
            $page = \View::make('errors.business.signin.input');
            $page->with('bc', ['business' => 'Business']);
            $page->with('nav', 'Business');
            $page->with('title', 'Input Validation Error');
        }

        return $page;
    }

    /**
     * @return $this
     */
    public function signOut()
    {
        $cookie = \Cookie::forget('signed-in');

        return \Redirect::to(\Utilities::URL('business'))
            ->withCookie($cookie);
    }
}
