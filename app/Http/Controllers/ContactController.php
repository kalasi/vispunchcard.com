<?php
namespace App\Http\Controllers;

/**
 * Class ContactController
 * @package App\Http\Controllers
 */
class ContactController extends BaseController
{
    /**
     * @param null $url
     *
     * @return \Illuminate\View\View
     */
    public function form($url = null)
    {
        $page = \View::make('contact.form');

        $page->with('title', 'Contact Us');

        if (!empty($url)) {
            $page->with('subject', 'Page Missing: ' . $url);
        }

        $page->with('css', 'rt-form');
        $page->with('js', 'rt-form');

        return $page;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function submit()
    {
        $email = \Input::get('email');
        $fname = \Input::get('fname');
        $subject = \Input::get('subject');
        $contents = \Input::get('undefined');
        $data = [
            'email'    => $email,
            'fname'    => $fname,
            'subject'  => $subject,
            'contents' => $contents,
        ];

        $requirements = [
            'email'    => 'required|email',
            'fname'    => 'required',
            'subject'  => 'required',
            'contents' => 'required|min:1'
        ];

        $validator = \Validator::make($data, $requirements);

        if ($validator->passes()) {
            \DB::table('messages')
                ->insertGetId([
                    'email'     => $email,
                    'fname'     => $fname,
                    'subject'   => $subject,
                    'contents'  => $contents,
                    'timestamp' => time()
                ]);

            $page = \View::make('contact.submit');

            $page->with('nav', 'Contact');
            $page->with('title', 'Message Sent');
        } else {
            $page = \View::make('errors.contact.input');

            $page->with('nav', 'Contact');
            $page->with('title', 'Error Sending Message');
            $page->with('errors', $validator->messages());
        }

        return $page;
    }
}
