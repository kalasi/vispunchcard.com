<?php
namespace App\Http\Controllers;

/**
 * Class AccountController
 * @package App\Http\Controllers
 */
class AccountController extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $businesses = \DB::table('customers')
            ->select('business', 'punches')
            ->where('user', \User::$id)
            ->get();

        $punchcards = [];

        foreach ($businesses as $business) {
            $businessInfo = \DB::table('businesses')
                ->select('name', 'city', 'municipality', 'punches_required')
                ->where('id', $business->business)
                ->first();

            array_push($punchcards, [
                'name'             => $businessInfo->name,
                'city'             => $businessInfo->city,
                'municipality'     => $businessInfo->municipality,
                'punches'          => $business->punches,
                'punches_required' => $businessInfo->punches_required,
                'full'             => floor($business->punches / $businessInfo->punches_required),
                'current'          => $business->punches % $businessInfo->punches_required,
            ]);
        }

        $businesses = \DB::table('businesses')
            ->select('id', 'name', 'city', 'municipality')
            ->where('owner', \User::$id)
            ->get();

        $employedByQuery = \DB::table('employees')
            ->select('business')
            ->where('user', \User::$id)
            ->get();

        $employedBy = [];

        foreach ($employedByQuery as $employed) {
            $employedInfo = \DB::table('businesses')
                ->select('id', 'name', 'city', 'municipality')
                ->where('id', $employed->business)
                ->first();

            array_push($employedBy,$employedInfo);
        }

        $page = \View::make('account.index');
        $page->with('nav', \User::name());
        $page->with('title', 'Account Info');
        $page->with('businesses', $businesses);
        $page->with('employedBy', $employedBy);
        $page->with('punchcards', $punchcards);

        return $page;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function punchcards()
    {
        $businesses = \DB::table('customers')
            ->select('business', 'punches')
            ->where('user', \User::$id)
            ->get();

        $punchcards = [];

        foreach ($businesses as $business) {
            $businessInfo = \DB::table('businesses')
                ->select('id', 'name', 'city', 'municipality', 'punches_required')
                ->where('id', $business->business)
                ->first();

            $transactions = \DB::table('transactions')
                ->select('punches', 'used', 'timestamp')
                ->where('business', $businessInfo->id)
                ->where('customer', \User::$id)
                ->orderBy('id','desc')
                ->get();

            array_push($punchcards, (object) [
                'id'                => $businessInfo->id,
                'name'              => $businessInfo->name,
                'city'              => $businessInfo->city,
                'municipality'      => $businessInfo->municipality,
                'punches'           => $business->punches,
                'punches_required'  => $businessInfo->punches_required,
                'full'              => floor($business->punches / $businessInfo->punches_required),
                'current'           => $business->punches % $businessInfo->punches_required,
                'transactions'      => $transactions,
                'transactionAmount' => count((array) $transactions),
            ]);
        }

        $page = \View::make('account.punchcards');

        $page->with('bc', ['account' => 'My Account']);
        $page->with('nav', \User::name());
        $page->with('title', 'My Punchcards');
        $page->with('punchcards', $punchcards);

        return $page;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function settings()
    {
        $settings = (object) [
            'fname' => (object) [
                'name'        => 'fname',
                'label'       => 'First Name',
                'placeholder' => \User::$fname,
                'type'        => 'text',
            ],
        ];

        $page = \View::make('account.settings.form');

        $page->with('bc', ['account' => 'My Account']);
        $page->with('nav', \User::name());
        $page->with('title', 'Account Settings');
        $page->with('settings', $settings);

        return $page;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function settingsChange()
    {
        $fname = \Input::get('fname');

        $changedSettings = [];

        if (!empty($fname)) {
            \DB::table('users')
                ->where('id', \User::$id)
                ->update([
                    'fname' => $fname,
                ]);

            $changedSettings['fname'] = [
                'name'  => 'First Name',
                'value' => $fname,
            ];

            \User::$fname = $fname;
        }

        $changedSettings = (object) json_decode(json_encode($changedSettings));

        $page = \View::make('account.settings.done');

        $page->with('bc', ['account' => 'My Account']);
        $page->with('nav', \User::name());
        $page->with('title', 'Account Settings Changed');
        $page->with('changedSettings', $changedSettings);

        return $page;
    }
}
