<?php
namespace App\Http\Controllers;

/**
 * Class LoginController
 * @package App\Http\Controllers
 */
class LoginController extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $page = \View::make('login.index');

        $page->with('nav','Login');
        $page->with('title','Login');

        return $page;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function businessIndex()
    {
        $page = \View::make('login.business.index');

        $page->with('title','Business Login');
        $page->with('css','rt-form');

        return $page;
    }

    /**
     * @return $this|\Illuminate\View\View
     */
    public function businessSubmitted()
    {
        $data = \Utilities::processPhone(\Input::get('phone'));

        $data = [
            'area'     => $data['area'],
            'exchange' => $data['exchange'],
            'sub'      => $data['sub'],
            'name'     => \Input::get('name'),
            'pin'      => \Input::get('pin'),
        ];

        $requirements = [
            'area'     => 'required|numeric|digits:3',
            'exchange' => 'required|numeric|digits:3',
            'sub'      => 'required|numeric|digits:4',
            'name'     => 'required',
            'pin'      => 'required|numeric|digits:4',
        ];

        $validator = \Validator::make($data,$requirements);

        if ($validator->passes()) {
            $businessInfo = \DB::table('businesses')
                ->select('id', 'owner', 'pin')
                ->where('name', $data['name'])
                ->first();

            if ($businessInfo) {
                $isOwner = \DB::table('users')
                    ->select('id')
                    ->where('area', $data['area'])
                    ->where('exchange', $data['exchange'])
                    ->where('sub', $data['sub'])
                    ->first();

                if ($isOwner) {
                    $info = (object) [
                        'id'  => $businessInfo->id,
                        'pin' => $data['pin'],
                    ];

                    $cookieBI = \Cookie::forever('business-info', \Crypt::encrypt(serialize($info)));
                    $cookieType = \Cookie::forever('user-type', 'business');

                    return \Redirect::to('business')
                        ->withCookie($cookieBI)
                        ->withCookie($cookieType);
                }
            }

            $page = \View::make('errors.login.business.invalid');

            $page->with('nav', 'Login');
            $page->with('title', 'Error Logging In');
        } else {
            $page = \View::make('errors.login.business.input');

            $page->with('nav', 'Login');
            $page->with('title', 'Error Logging In');
        }

        return $page;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function userIndex()
    {
        $page = \View::make('login.user.index');

        $page->with('title', 'Login');
        $page->with('css', 'rt-form');

        return $page;
    }

    /**
     * @return $this|\Illuminate\View\View
     */
    public function userSubmitted()
    {
        $data = \Utilities::processPhone(\Input::get('phone'));
        $data = [
            'area'     => $data['area'],
            'exchange' => $data['exchange'],
            'sub'      => $data['sub'],
        ];

        $requirements = [
            'area'     => 'required|numeric',
            'exchange' => 'required|numeric',
            'sub'      => 'required|numeric',
        ];

        $validator = \Validator::make($data,$requirements);

        if ($validator->passes()) {
            $userData = [
                'area'     => $data['area'],
                'exchange' => $data['exchange'],
                'sub'      => $data['sub'],
                'password' => 'pass',
            ];

            if (\Auth::attempt($userData, true)) {
                $cookie = \Cookie::forever('user-type', 'user');

                return \Redirect::to('account')
                    ->withCookie($cookie);
            } else {
                $page = \View::make('errors.login.user.invalid');

                $page->with('nav', 'Login');
                $page->with('title', 'Error Logging In');
            }
        } else {
            $page = \View::make('errors.login.user.input');

            $page->with('nav', 'Login');
            $page->with('title', 'Error Logging In');
        }

        return $page;
    }
}
