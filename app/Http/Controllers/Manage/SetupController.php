<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\BaseController;
use App\User;

/**
 * Class SetupController
 * @package App\Http\Controllers\Manage
 */
class SetupController extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function form()
    {
        $page = \View::make('manage.setup.form');

        $page->with('title', 'Setup Your Business');
        $page->with('titleSub', 'With Punchcards');
        $page->with('css', 'rt-form');
        $page->with('js', 'rt-form');

        return $page;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function submitted()
    {
        $name = \Input::get('name');
        $city = \Input::get('city');
        $municipality = \Input::get('municipality');
        $country = 'usa';
        $pin = \Input::get('pin');
        $punchesRequired = \Input::get('punches_required');
        $data = [
            'name'             => $name,
            'city'             => $city,
            'municipality'     => $municipality,
            'country'          => $country,
            'pin'              => $pin,
            'punches_required' => $punchesRequired,
        ];

        $requirements = [
            'name'             => 'required',
            'city'             => 'required',
            'municipality'     => 'required',
            'country'          => 'required|size:3',
            'pin'              => 'required|min:4|max:4',
            'punches_required' => 'required',
        ];

        $validator = \Validator::make($data,$requirements);

        if ($validator->passes()) {
            $success = \DB::table('businesses')
                ->insertGetId([
                    'name'             => $name,
                    'city'             => $city,
                    'municipality'     => $municipality,
                    'country'          => $country,
                    'pin'              => $pin,
                    'punches_required' => $punchesRequired,
                    'owner'            => User::$id,
                    'punches'          => 0,
                    'punchcards_used'  => 0,
                    'transactions'     => 0,
                ]);

            $id = \DB::table('businesses')
                ->select('id')
                ->orderBy('id', 'desc')
                ->first();

            if ($success) {
                return \Redirect::to('manage/' . $id->id);
            } else {
                $page = \View::make('errors.manage.setup.db');

                $page->with('title', 'Error Setting Up');
                $page->with('titleSub', 'Database Error');
            }
        } else {
            $page = \View::make('errors.manage.setup.input');

            $page->with('title', 'Error Setting Up');
            $page->with('titleSub', 'Validation Error');
        }

        return $page;
    }
}
