<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\BaseController;

/**
 * Class BusinessController
 * @package App\Http\Controllers\Manage
 */
class BusinessController extends BaseController
{
    /**
     * @param $id
     *
     * @return \Illuminate\View\View
     */
    public function index($id)
    {
        $business = \Business::info($id);
        if ($business) {
            $permissions = \Business::permissions($id);
            if ($permissions->owner || $permissions->admin) {
                if ($permissions->pin) {
                    $adminQuery = \DB::table('admins')
                        ->select('user')
                        ->where('business', $business->id)
                        ->get();

                    $admins = [];

                    $employees = [];

                    foreach ($adminQuery as $admin) {
                        if (!empty($admin)) {
                            $userInfo = \DB::table('users')
                                ->select('id', 'fname', 'area', 'exchange', 'sub')
                                ->where('id', $admin->user)
                                ->first();

                            if (!empty($userInfo)) {
                                array_push($admins, $userInfo);
                            }
                        }
                    }

                    $employeesQuery = \DB::table('employees')
                        ->select('user')
                        ->where('business', $business->id)
                        ->get();

                    foreach ($employeesQuery as $employee) {
                        if (!empty($employee)) {
                            $employeeInfo = \DB::table('users')
                                ->select('id', 'fname', 'area', 'exchange', 'sub')
                                ->where('id', $employee->user)
                                ->first();

                            if (!empty($employeeInfo)) {
                                array_push($employees, $employeeInfo);
                            }
                        }
                    }

                    $customers = \DB::table('customers')
                        ->where('business', $business->id)
                        ->count();

                    $addresses = \DB::table('customers')
                        ->where('business', $business->id)
                        ->where('email', '!=', '')
                        ->count();

                    $page=\View::make('manage.view');

                    $page->with('bc', ['manage' => 'Management Center']);
                    $page->with('nav', 'Manage');
                    $page->with('title', $business->name);
                    $page->with('titleSub', 'Viewing');
                    $page->with('addresses', $addresses);
                    $page->with('admins', $admins);
                    $page->with('business', $business);
                    $page->with('customers', $customers);
                    $page->with('employees', $employees);
                    $page->with('isOwner', $permissions->owner);
                    $page->with('punches', $business->punches);
                    $page->with('transactions', $business->transactions);
                    $page->with('used', $business->punchcards_used);
                } else {
                    $page = \View::make('manage.pin');

                    $page->with('title', 'Enter Business PIN');
                    $page->with('css', 'rt-form');
                    $page->with('js', 'rt-form');
                    $page->with('business', $business);
                }
            } else {
                $page = \View::make('errors.manage.invalidPermissions');

                $page->with('title', 'Error Managing Business');
                $page->with('titleSub', 'Not Owner');
                $page->with('business', $business);
            }
        } else {
            $page = \View::make('errors.manage.view.missing');
            $page->with('title', 'Error Managing Business');
            $page->with('titleSub', 'Nonexistent');
            $page->with('id', $id);
        }

        return $page;
    }

    /**
     * @param $id
     *
     * @return $this|\Illuminate\View\View
     */
    public function pin($id)
    {
        $pin = \Input::get('pin');
        $business = \Business::info($id);
        if ($pin == $business->pin) {
            $cookie = \Cookie::make('u' . \User::$id . 'b' . $id . 'p', $pin);

            return \Redirect::to('manage/' . $id)
                ->withCookie($cookie);
        } else {
            $page = \View::make('errors.manage.pinIncorrect');

            $page->with('bc', ['manage' => 'Management Center']);
            $page->with('nav', 'Manage');
            $page->with('title', 'Error Managing Business');
            $page->with('titleSub', 'Invalid PIN');
            $page->with('business', $business);
        }

        return $page;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\View\View
     */
    public function addresses($id)
    {
        $permissions = \Business::permissions($id);
        if (!$permissions->owner || $permissions->admin) {
            abort(403);
        }

        $business = \Business::info($id);

        $addresses = \DB::table('customers')
            ->select('email')
            ->where('business', $id)
            ->where('email', '!=', '')
            ->get();

        $array = [];

        $csv = "#,Email<br />";
        $plain = "";

        $x = 1;

        foreach ($addresses as $address) {
            array_push($array, $address->email);

            $csv .= $x . "," . $address->email . "<br />";
            $plain .= "<p>" . $address->email . "</p>";

            $x++;
        }

        $json = json_encode($array);

        $page = \View::make('manage.addresses');

        $page->with('bc', ['manage' => 'Management Center', 'manage/' . $business->id => $business->name]);
        $page->with('nav', 'Manage');
        $page->with('title', 'Addresses');
        $page->with('json', $json);
        $page->with('csv', $csv);
        $page->with('plain', $plain);

        return $page;
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function switchFrom($id)
    {
        $businessInfo = \Business::info($id);

        \Auth::logout();

        $info = (object) [
            'id'  => $businessInfo->id,
            'pin' => $businessInfo->pin,
        ];

        $cookieBI = \Cookie::forever('business-info', \Crypt::encrypt(serialize($info)));
        $cookieType = \Cookie::forever('user-type', 'business');

        return \Redirect::to('business')
            ->withCookie($cookieBI)
            ->withCookie($cookieType);
    }
}
