<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\BaseController;
use App\User;

/**
 * Class StaffController
 * @package App\Http\Controllers\Manage
 */
class StaffController extends BaseController
{
    /**
     * @param $id
     * @param $type
     *
     * @return \Illuminate\View\View
     */
    public function index($id, $type)
    {
        $permissions = \Business::permissions($id);

        $role = \Business::matchingRole($type);

        if ($permissions->owner || $permissions->admin) {
            $staffQuery = \DB::table($role->name_trims)
                ->select('user')
                ->where('business', $id)
                ->get();

            $business = \Business::info($id);

            $staffs = [];

            foreach ($staffQuery as $user) {
                $userInfo = \DB::table('users')
                    ->select('id', 'fname', 'area', 'exchange', 'sub')
                    ->where('id', $user->user)
                    ->first();

                array_push($staffs,$userInfo);
            }

            $page = \View::make('manage.staff.index');

            $page->with('bc', ['manage' => 'Management Center', 'manage/' . $business->id => $business->name]);
            $page->with('nav', 'Manage');
            $page->with('title', 'Manage ' . $role->names);
            $page->with('staffs', $staffs);
            $page->with('business', $business);
            $page->with('role', $role);
        } else {
            return \Business::invalidPermissions();
        }

        return $page;
    }

    /**
     * @param $id
     * @param $type
     *
     * @return \Illuminate\View\View
     */
    public function addForm($id, $type)
    {
        $permissions = \Business::permissions($id);
        $role = \Business::matchingRole($type);
        if ($permissions->owner || $permissions->admin) {
            $business = \Business::info($id);
            $page = \View::make('manage.staff.add.form');
            $bc = [
                'manage'                                           => 'Management Center',
                'manage/' . $business->id                          => $business->name,
                'manage/' . $business->id . '/' . $role->name_trim => 'Manage ' . $role->names,
            ];

            $page->with('bc', $bc);
            $page->with('title', 'Add '.$role->name);
            $page->with('css', 'rt-form');
            $page->with('js', 'rt-form');
            $page->with('role', $role);
        } else {
            return \Business::invalidPermissions();
        }

        return $page;
    }

    /**
     * @param $id
     * @param $type
     *
     * @return \Illuminate\View\View
     */
    public function addDone($id, $type) {
        $permissions = \Business::permissions($id);
        $role = \Business::matchingRole($type);
        if ($permissions->owner || $permissions->admin) {
            $business = \Business::info($id);
            $data = \Utilities::processPhone(\Input::get('phone'));

            $requirements = [
                'area'     => 'required|numeric',
                'exchange' => 'required|numeric',
                'sub'      => 'required|numeric',
            ];

            $validator = \Validator::make($data,$requirements);

            if ($validator->passes()) {
                $userExists = \DB::table('users')
                    ->select('id')
                    ->where('area', $data['area'])
                    ->where('exchange', $data['exchange'])
                    ->where('sub', $data['sub'])
                    ->first();

                if ($userExists) {
                    $alreadyStaff = \DB::table($role->name_trims)
                        ->select('id')
                        ->where('user', $userExists->id)
                        ->where('business', $business->id)
                        ->first();

                    if(!$alreadyStaff){
                        \DB::table($role->name_trims)
                            ->insertGetId([
                                'user'          => $userExists->id,
                                'business'      => $business->id,
                                'authorized_by' => User::$id,
                                'validated'     => 0,
                            ]);
                    }

                    $page = \View::make('manage.staff.add.done');
                    $bc = [
                        'manage'                                            => 'Management Center',
                        'manage/' . $business->id                           => $business->name,
                        'manage/' . $business->id . '/' . $role->name_trims => 'Manage ' . $role->names,
                    ];

                    $page->with('bc', $bc);
                    $page->with('nav', 'Manage');
                    $page->with('title', 'Added ' . $role->name);
                    $page->with('area', $data['area']);
                    $page->with('business', $business);
                    $page->with('exchange', $data['exchange']);
                    $page->with('sub', $data['sub']);
                    $page->with('role', $role);
                } else {
                    /**
                     * Create new User to add as Administrator
                     */
                    $page = $this->createName($id, $type, $data);
                }
            } else {
                $page = \View::make('errors.manage.admins.add.input');

                $bc = [
                    'manage'                              => 'Management Center',
                    'manage/' . $business->id             =>$business->name,
                    'manage/' . $business->id . '/admins' =>'Manage Admins',
                ];

                $page->with('bc', $bc);
                $page->with('title', 'Error Adding Employee');
                $page->with('titleSub', 'Input Validation');
            }
        } else{
            return \Business::invalidPermissions();
        }

        return $page;
    }

    /**
     * @param $id
     * @param $type
     * @param $userID
     *
     * @return \Illuminate\View\View
     */
    public function remove($id, $type, $userID)
    {
        $permissions = \Business::permissions($id);

        $role = \Business::matchingRole($type);

        if ($permissions->owner) {
            $business = \Business::info($id);

            $exists = \DB::table($role->name_trims)
                ->select('id')
                ->where('user', $userID)
                ->where('business', $id)
                ->first();

            if ($exists) {
                $userInfo = \DB::table('users')
                    ->select('fname')
                    ->where('id', $userID)
                    ->first();

                \DB::table($role->name_trims)
                    ->where('id', $exists->id)
                    ->delete();

                $page = \View::make('manage.staff.remove');

                $page->with('bc', ['manage' => 'Management Center', 'manage/' . $business->id => $business->name]);
                $page->with('nav', 'Manage');
                $page->with('title', 'Management');
                $page->with('titleSub', $role->name . ' Removed');
                $page->with('fname', $userInfo->fname);
            } else {
                $page = \View::make('errors.manage.staff.remove.notFound');
                $page->with('title', 'Management');
                $page->with('titleSub', 'Error');
                $page->with('role', $role);
                $page->with('id', $userID);
            }

            $page->with('role', $role);
            $page->with('businessID', $id);
        } else {
            return \Business::invalidPermissions();
        }

        return $page;
    }

    /**
     * @param $id
     * @param $type
     * @param $data
     *
     * @return \Illuminate\View\View
     */
    public function createName($id, $type, $data)
    {
        $business = \Business::info($id);
        $role = \Business::matchingRole($type);

        $page = \View::make('manage.staff.create.name');

        $page->with('title', 'New User');
        $page->with('css', 'rt-form');
        $page->with('js', 'rt-form');
        $page->with('business', $business);
        $page->with('phone', \Crypt::encrypt(json_encode($data)));
        $page->with('role', $role);

        return $page;
    }

    /**
     * @param $id
     * @param $type
     *
     * @return \Illuminate\View\View
     */
    public function createUser($id, $type)
    {
        $data = json_decode(\Crypt::decrypt(\Input::get('phone')));

        $name = \Input::get('name');

        $data = [
            'area'     => $data->area,
            'exchange' => $data->exchange,
            'sub'      => $data->sub,
            'name'     => $name,
        ];

        $requirements = [
            'area'      => 'required|size:3',
            'exchange'  => 'required|size:3',
            'sub'       => 'required|size:4|min:0',
            'name'      => 'required',
        ];

        $validator = \Validator::make($data,$requirements);

        if (!$validator->passes()) {
            abort(403);
        }

        $role = \Business::matchingRole($type);
        $business = \Business::info($id);
        $hashedPassword = \Hash::make('pass');

        $user = new User;

        $user->fname = $name;
        $user->area = $data['area'];
        $user->exchange = $data['exchange'];
        $user->sub = $data['sub'];
        $user->password = $hashedPassword;
        $user->save();

        \DB::table($role->name_trims)
            ->insertGetId([
                'user'          => $user->id,
                'business'      => $business->id,
                'authorized_by' => User::$id,
                'validated'     => 0,
            ]);

        $page = \View::make('manage.staff.add.done');

        $bc = [
            'manage'                                           => 'Management Center',
            'manage/' . $business->id                          => $business->name,
            'manage/' . $business->id . '/' . $role->name_trim => 'Manage ' . $role->names,
        ];

        $page->with('bc', $bc);
        $page->with('nav', 'Manage');
        $page->with('title', 'Added ' . $role->name);
        $page->with('area', $data['area']);
        $page->with('business', $business);
        $page->with('exchange', $data['exchange']);
        $page->with('sub', $data['sub']);
        $page->with('role', $role);

        return $page;
    }
}
