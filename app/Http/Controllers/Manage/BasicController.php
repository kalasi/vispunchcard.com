<?php
namespace App\Http\Controllers\Manage;

use App\Http\Controllers\BaseController;

/**
 * Class BasicController
 * @package App\Http\Controllers\Manage
 */
class BasicController extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $businesses = [];
        $employed = [];

        $businessQuery = \DB::table('businesses')
            ->select('id', 'name', 'city', 'municipality', 'country')
            ->where('owner', \User::$id)
            ->get();

        $admins = \DB::table('admins')
            ->select('business')
            ->where('user', \User::$id)
            ->get();

        foreach ($businessQuery as $business) {
            $arr = [
                'id'           => $business->id,
                'name'         => $business->name,
                'city'         => $business->city,
                'municipality' => $business->municipality,
                'country'      => $business->country,
            ];

            array_push($businesses, $arr);
        }

        foreach ($admins as $admin) {
            $businessInfo = \DB::table('businesses')
                ->select('id', 'name', 'city', 'municipality', 'country')
                ->where('id', $admin->business)
                ->first();

            $arr = [
                'id'           => $businessInfo->id,
                'name'         => $businessInfo->name,
                'city'         => $businessInfo->city,
                'municipality' => $businessInfo->municipality,
                'country'      => $businessInfo->country,
            ];

            array_push($businesses,$arr);
        }

        $employment = \DB::table('employees')
            ->select('business')
            ->where('user', \User::$id)
            ->get();

        foreach ($employment as $employ) {
            $businessInfo = \DB::table('businesses')
                ->select('id', 'name', 'city', 'municipality', 'country')
                ->where('id', $employ->business)
                ->first();

            $arr = [
                'id'           => $businessInfo->id,
                'name'         => $businessInfo->name,
                'city'         => $businessInfo->city,
                'municipality' => $businessInfo->municipality,
                'country'      => $businessInfo->country,
            ];

            array_push($employed,$arr);
        }

        $page = \View::make('manage.index');

        $page->with('nav', 'Manage');
        $page->with('title', 'Management Center');
        $page->with('titleSub', 'Businesses');
        $page->with('businesses', $businesses);
        $page->with('employed', $employed);

        return $page;
    }
}
