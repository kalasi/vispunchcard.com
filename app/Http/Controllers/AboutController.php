<?php
namespace App\Http\Controllers;

/**
 * Class AboutController
 * @package App\Http\Controllers
 */
class AboutController extends BaseController
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $hash = \Crypt::encrypt(microtime(true));

        $page = \View::make('about');

        $page->with('hash', $hash);
        $page->with('nav', 'About');
        $page->with('title', 'About Us');

        return $page;
    }
}
