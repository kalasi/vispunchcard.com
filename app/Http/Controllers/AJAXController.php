<?php
namespace App\Http\Controllers;

/**
 * Class AJAXController
 * @package App\Http\Controllers
 */
class AJAXController extends BaseController
{
    /**
     * @return string
     */
    public function gtld()
    {
        return json_encode([
            'com', 'org', 'net', 'int', 'edu', 'gov', 'mil',
            'ac', 'ad', 'ae', 'af', 'ag', 'ai', 'al', 'am', 'an', 'ao', 'aq', 'ar',
            'as', 'at', 'au', 'aw', 'ax', 'az',
            'ba', 'bb', 'cd', 'be', 'bf', 'bg', 'bh', 'bi', 'bj', 'bm', 'bn', 'bo',
            'br', 'bs', 'bt', 'bv', 'bw', 'by', 'bz',
            'ca', 'cc', 'cd', 'cf', 'cg', 'ch', 'ci', 'ck', 'cl', 'cm', 'cn', 'cr',
            'cs', 'cu', 'cv', 'cw', 'cx', 'cy', 'cz',
            'de', 'dj', 'dk', 'dm', 'do', 'dz',
            'ec', 'ee', 'eg', 'eh', 'er', 'es', 'et', 'eu',
            'fi', 'fj', 'fk', 'fm', 'fo', 'fr',
            'ga', 'gb', 'gd', 'ge', 'gf', 'gg', 'gh', 'gi', 'gl', 'gm', 'gn', 'gp',
            'gq', 'gr', 'gs', 'gt', 'gu', 'gw', 'gy',
            'hk', 'hm', 'hn', 'hr', 'ht', 'hu',
            'id', 'ie', 'il', 'im', 'in', 'io', 'iq', 'ir', 'is', 'it',
            'je', 'jm', 'jo', 'jp',
            'ke', 'kg', 'kh', 'ki', 'km', 'kn', 'kp', 'kr', 'kw', 'ky', 'kz',
            'la', 'lb', 'lc', 'li', 'lk', 'lr', 'ls', 'lt', 'lu', 'lv', 'ly',
            'ma', 'mc', 'md', 'me', 'mg', 'mh', 'mk', 'ml', 'mm', 'mp', 'mq',
            'mr', 'ms', 'mt', 'mu', 'mv', 'mw', 'mx', 'my', 'mz',
            'na', 'nc', 'nf', 'ng', 'ni', 'nl', 'no', 'np', 'nr', 'nu', 'nz',
            'om',
            'pa', 'pe', 'pf', 'pg', 'ph', 'pk', 'pl', 'pm', 'pn', 'pr', 'ps', 'pt',
            'pw', 'py',
            'qa',
            're', 'ro', 'rs', 'ru', 'rw',
            'sa', 'sb', 'sc', 'sd', 'se', 'sg', 'sh', 'si', 'sj', 'sk', 'sl', 'sm',
            'sn', 'so', 'sr', 'ss', 'st', 'su', 'sv', 'sx', 'sy', 'sz',
            'tc', 'td', 'tf', 'tg', 'th', 'tj', 'tk', 'tl', 'tm', 'tn', 'to', 'tp',
            'tr', 'tt', 'tv', 'tw', 'tz',
            'ua', 'ug', 'uk', 'us', 'uy', 'uz',
            'va', 'vc', 've', 'vg', 'vi', 'vn', 'vu', 'wf', 'ws',
            'ye', 'yt',
            'za', 'zm', 'zw',
            'academy', 'aero', 'agency', 'associates',
            'bar', 'bargains', 'bid', 'bike', 'biz', 'blackfriday', 'blue',
            'boutique', 'build', 'builders', 'buzz',
            'cab', 'camera', 'camp', 'capital', 'cards', 'catering',
            'careers', 'center', 'ceo', 'cheap', 'christmas', 'cleaning',
            'clothing', 'club', 'codes', 'coffee', 'college', 'community',
            'company', 'computer', 'condos', 'constructions', 'consulting',
            'contractors', 'cool', 'coop', 'cruises',
            'dance', 'date', 'dating', 'democrat', 'diamonds', 'directory',
            'domains',
            'education', 'email', 'engineering', 'enterprises', 'equipment',
            'estate', 'eus', 'events', 'exchange', 'expert',
            'farm', 'fish', 'flights', 'florist', 'foo', 'futbol',
            'gallery', 'gift', 'glass', 'graphics', 'gripe', 'guitars',
            'guru',
            'holdings', 'holiday', 'house',
            'immobilien', 'industries', 'info', 'ink', 'institute',
            'international',
            'jetzt', 'jobs',
            'kaufen', 'kitchen', 'kim',
            'land', 'lease', 'lighting', 'limo', 'link',
            'maison', 'management', 'marketing', 'media', 'menu', 'mobi',
            'moda', 'moe', 'museum',
            'name', 'ninja',
            'onl',
            'partners', 'parts', 'photo', 'photography', 'photos', 'pics',
            'pictures', 'pink', 'plumbing', 'post', 'pro', 'productions',
            'products', 'properties', 'pub',
            'recipes', 'red', 'reisen', 'rentals', 'repair', 'report', 'rest',
            'reviews', 'rich',
            'services', 'shiksha', 'shoes', 'social', 'solar', 'solutions',
            'space', 'supplies', 'supply', 'support', 'systems',
            'tattoo', 'technology', 'tel', 'tienda', 'tips', 'today', 'tools',
            'town', 'toys', 'trade', 'training', 'travel',
            'university', 'uno',
            'vacations', 'ventures', 'viajes', 'villas', 'vision', 'voting',
            'voyage',
            'watch', 'webcam', 'wed', 'wiki', 'works',
            'xyz',
            'zone',
            'asia', 'berlin', 'cat', 'gal', 'kiwi', 'london', 'nagoya', 'nyc',
            'paris', 'tokyo', 'wien',
        ]);
    }
}
