<?php
namespace App\Http\Controllers;

use App\User;

/**
 * Class CheckoutController
 * @package App\Http\Controllers
 */
class CheckoutController extends BaseController
{
    /**
     * @param $id
     *
     * @return \Illuminate\View\View
     */
    public function index($id)
    {
        $business = \DB::table('businesses')
            ->select('id', 'name', 'city', 'municipality', 'pin')
            ->where('id', $id)
            ->first();

        $page = \View::make('checkout.index');

        $page->with('nav', 'Checkout');
        $page->with('css', 'checkout');
        $page->with('js', 'phone-form');
        $page->with('business', $business);

        return $page;
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\View\View
     */
    public function confirm($id = '')
    {
        $userID = \Input::get('user_id');
        if (!$userID) {
            $userID = \Session::get('checkout-customer-id');
            \Session::forget('checkout-customer-id');
        }

        $business = \DB::table('businesses')
            ->select('id','name','city','municipality','punches_required')
            ->where('id',$id)
            ->first();

        $data = \Utilities::processPhone(\Input::get('phone'));

        $requirements = [
            'area'     => 'required|numeric',
            'exchange' => 'required|numeric',
            'sub'      => 'required|numeric',
        ];

        $validator=\Validator::make($data,$requirements);

        if ($validator->passes() || $userID) {
            if ($userID) {
                $user = \DB::table('users')
                    ->select('id', 'fname')
                    ->where('id', $userID)
                    ->first();
            } else {
                $user = \DB::table('users')
                    ->select('id', 'fname')
                    ->where('area', $data['area'])
                    ->where('exchange', $data['exchange'])
                    ->where('sub', $data['sub'])
                    ->first();
            }

            if ($user) {
                $customer = \DB::table('customers')
                    ->select('punches')
                    ->where('business', $id)
                    ->where('user', $user->id)
                    ->first();

                if (!$customer) {
                    \DB::table('customers')
                        ->insertGetId([
                            'business' => $id,
                            'user'     => $user->id,
                            'email'    => '',
                            'punches'  => 0,
                        ]);

                    $customer = \DB::table('customers')
                        ->select('punches')
                        ->where('business', $id)
                        ->where('user', $user->id)
                        ->first();
                }

                $punchesCurrent = $customer->punches % $business->punches_required;
                $full = floor($customer->punches / $business->punches_required);

                $page = \View::make('checkout.confirm');

                $page->with('nav','Checkout');
                $page->with('css','checkout');
                $page->with('js','checkout');
                $page->with('user',$user);
                $page->with('business',$business);
                $page->with('customer',$customer);
                $page->with('user',$user);
                $page->with('punchesCurrent',$punchesCurrent);
                $page->with('full',$full);
            } else {
                $page = \View::make('checkout.customer.create');

                $page->with('nav', 'Checkout');
                $page->with('title', 'New User');
                $page->with('titleSub', 'Sign Up');
                $page->with('phone', \Crypt::encrypt(json_encode($data)));
                $page->with('business', $business);
            }
        } else {
            $page = \View::make('errors.checkout.userInvalid');
            $page->with('title', 'Invalid User');
            $page->with('business', $business);
        }

        return $page;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\View\View
     */
    public function complete($id)
    {
        $userID = \Input::get('user_id');
        $new = \Input::get('new');
        $used = \Input::get('used');

        $business = \DB::table('businesses')
            ->select('id', 'name', 'city', 'municipality', 'punches_required', 'punches', 'punchcards_used', 'transactions')
            ->where('id', $id)
            ->first();

        $customer = \DB::table('customers')
            ->select('punches')
            ->where('business', $id)
            ->where('user', $userID)
            ->first();

        $newPunches = $customer->punches - ($used * $business->punches_required) + $new;

        \DB::table('customers')
            ->where('user', $userID)
            ->where('business', $business->id)
            ->update([
                'punches' => $newPunches,
            ]);

        \DB::table('businesses')
            ->where('id', $business->id)
            ->update([
                'punches'         => $business->punches + $new,
                'punchcards_used' => $business->punchcards_used + $used,
                'transactions'    => $business->transactions + 1,
            ]);

        \DB::table('transactions')
            ->insertGetId([
                'business'  => $business->id,
                'customer'  => $userID,
                'employee'  => \Business::signedInInfo()->id,
                'punches'   => $new,
                'used'      => $used,
                'timestamp' => time(),
            ]);

        $newCustomer = \DB::table('customers')
            ->select('punches', 'email')
            ->where('business', $id)
            ->where('user', $userID)
            ->first();

        $user = \DB::table('users')
            ->select('id', 'fname')
            ->where('id', $userID)
            ->first();

        $page = \View::make('checkout.complete');

        $page->with('nav','Checkout');

        $full = floor($newPunches / $business->punches_required);
        $current = $newPunches % $business->punches_required;

        $page->with('css', 'checkout');
        $page->with('business', $business);
        $page->with('customer', $newCustomer);
        $page->with('user', $user);
        $page->with('current', $current);
        $page->with('old', $customer->punches);
        $page->with('new', $newCustomer->punches - $customer->punches);
        $page->with('full', $full);

        return $page;
    }

    /**
     * @param $id
     *
     * @return \Illuminate\View\View
     */
    public function restart($id)
    {
        $email = \Input::get('email');
        $userID = \Input::get('user_id');

        $check = \DB::table('customers')
            ->select('email')
            ->where('user', $userID)
            ->pluck('email');

        if (empty($check)){
            \DB::table('customers')
                ->where('user', $userID)
                ->update([
                    'email' => $email,
                ]);
        }

        return $this->index($id);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create($id)
    {
        $phone = json_decode(\Crypt::decrypt(\Input::get('data')));
        $fname = \Input::get('fname');

        $data = [
            'fname' => $fname,
        ];

        $requirements = [
            'fname' => 'required',
        ];

        $validator = \Validator::make($data,$requirements);

        if ($validator->passes()) {
            $business = \DB::table('businesses')
                ->select('id', 'name')
                ->where('id', $id)
                ->first();

            $alreadyExists = \DB::table('users')
                ->select('id')
                ->where('area', $phone->area)
                ->where('exchange', $phone->exchange)
                ->where('sub', $phone->sub)
                ->first();

            if (!$alreadyExists) {
                $user = new User;

                $user->fname = $fname;
                $user->area = $phone->area;
                $user->exchange = $phone->exchange;
                $user->sub = $phone->sub;
                $user->password = \Hash::make('pass');

                $user->save();

                $userID = $user->id;
            } else {
                $userID = $alreadyExists->id;
            }

            \Session::put('checkout-customer-id', $userID);

            return \Redirect::to(\Utilities::URLBusiness($business->id, 'confirm'));
        } else {
            $page = \View::make('errors.checkout.customer.input');
        }

        return $page;
    }
}
