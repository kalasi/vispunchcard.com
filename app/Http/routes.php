<?php
/**
 * About
 */
Route::get('about', 'AboutController@index');

/**
 * Ajax
 */
Route::group(['prefix' => 'ajax'], function() {
    Route::get('gtld', 'AJAXController@gtld');
});

/**
 * Account
 */
Route::group(['prefix' => 'account'], function() {
    Route::get('/', 'AccountController@index');
    Route::get('punchcards', 'AccountController@punchcards');
    Route::get('settings', 'AccountController@settings');
    Route::post('settings', 'AccountController@settingsChange');
});

/**
 * Business
 */
Route::group(['middleware' => 'auth.loggedBusiness', 'prefix' => 'business'], function() {
    Route::get('/', 'BusinessController@index');
    Route::get('sign-in/{id}', 'BusinessController@signIn');
    Route::post('sign-in/{id}', 'BusinessController@signInDone');
    Route::get('sign-out', 'BusinessController@signOut');
});

/**
 * Checkout
 */
Route::group(['middleware' => 'business.signedIn', 'prefix' => 'checkout'],function(){
    Route::group(['prefix' => '{id}/{name}'], function() {
        Route::get('/', 'CheckoutController@index');
        Route::post('/', 'CheckoutController@restart');
        Route::get('pin', 'CheckoutController@pinForm');
        Route::post('pin', 'CheckoutController@pinDone');
        Route::get('confirm', 'CheckoutController@confirm');
        Route::post('confirm', 'CheckoutController@confirm');
        Route::post('complete', 'CheckoutController@complete');
        Route::post('create', 'CheckoutController@create');
    });
});

/**
 * Contact
 */
Route::group(['prefix' => 'contact'], function() {
    Route::get('/', 'ContactController@form');
    Route::get('page={url?}', 'ContactController@form')
        ->where('url', '(.*)');
    Route::post('/', 'ContactController@submit');
});

/**
 * Easter Eggs
 */
Route::get('save/{princess}', 'EasterEggsController@princess')
    ->where('princess', '[A-Za-z]+');

/**
 * Errors
 */
Route::group(['prefix' => 'error'], function() {
    Route::get('logged', 'ErrorController@logged');
    Route::get('not-business', 'ErrorController@notBusiness');
    Route::get('not-user', 'ErrorController@notUser');
});

/**
 * Home
 */
Route::get('/', 'HomeController@getIndex');

/**
 * Login
 */
Route::group(['middleware' => 'auth.guest', 'prefix'=>'login'], function() {
    Route::get('/', 'LoginController@index');

    /**
     * User
     */
    Route::group(['prefix' => 'user'],function() {
        Route::get('/', 'LoginController@userIndex');
        Route::post('/', 'LoginController@userSubmitted');
    });

    /**
     * Business
     */
    Route::group(['prefix' => 'business'], function() {
        Route::get('/', 'LoginController@businessIndex');
        Route::post('/', 'LoginController@businessSubmitted');
    });
});

/**
 * Logout
 */
Route::group(['prefix' => 'logout'], function() {
    Route::get('/','LogoutController@index');
    Route::get('done', 'LogoutController@done');
    Route::get('to={url?}','LogoutController@index')->where('url','(.*)');
});

/**
 * Manage
 */
Route::group(['prefix' => 'manage'], function() {
    Route::get('/', 'Manage\BasicController@index');

    /**
     * Setup a business
     */
    Route::group(['prefix' => 'setup'], function() {
        Route::get('/', 'Manage\SetupController@form');
        Route::post('/', 'Manage\SetupController@submitted');
    });

    /**
     * View a business
     */
    Route::group(['prefix' => '{id}'], function() {
        Route::get('/', 'Manage\BusinessController@index');
        Route::post('/', 'Manage\BusinessController@pin');
        Route::get('addresses', 'Manage\BusinessController@addresses');
        Route::get('edit', 'Manage\BusinessController@editForm');
        Route::post('edit', 'Manage\BusinessController@editDone');
        Route::post('pin', 'Manage\BusinessController@pin');
        Route::get('switch', 'Manage\BusinessController@switchFrom');
        Route::group(['prefix' => '{staff}'], function() {
            Route::get('/', 'Manage\StaffController@index');
            Route::get('add', 'Manage\StaffController@addForm');
            Route::post('add', 'Manage\StaffController@addDone');
            Route::get('{userID}/remove', 'Manage\StaffController@remove');
            Route::get('{userID}/stats', 'Manage\StaffController@stats');
            Route::post('create', 'Manage\StaffController@createUser');
        });
    });
});

/**
 * Sign Up
 */
Route::group(['middleware' => 'auth.guest', 'prefix' => 'signup'], function() {
    Route::get('/', 'SignUpController@index');
    Route::post('/', 'SignUpController@submitted');
});
