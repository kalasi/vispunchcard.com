<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class BusinessSignedIn
 * @package App\Http\Middleware
 */
class BusinessSignedIn
{
    /**
     * Handle an incoming request.
     *
     * @param          $request
     * @param callable $next
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        if (!\Business::signedIn()) {
            return redirect()->to('business');
        }

        return $next($request);
    }
}
