<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class AuthenticateBusiness
 * @package App\Http\Middleware
 */
class AuthenticateBusiness
{
    /**
     * Handle an incoming request.
     *
     * @param          $request
     * @param callable $next
     *
     * @return $this
     */
    public function handle($request, Closure $next)
    {
        $isBusiness = \Business::isBusiness();

        if (!$isBusiness) {
            $cookieBI = \Cookie::forget('business-info');
            $cookieType = \Cookie::forget('user-type');

            return \Redirect::to('login')
                ->withCookie($cookieBI)
                ->withCookie($cookieType);
        }

        return $next($request);
    }
}
