<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;

/**
 * Class RedirectIfAuthenticated
 * @package App\Http\Middleware
 */
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param          $request
     * @param callable $next
     *
     * @return RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check()) {
            return redirect()->to('/');
        }

        return $next($request);
    }
}
