<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class AuthenticatedOut
 * @package App\Http\Middleware
 */
class AuthenticatedOut
{
    /**
     * Handle an incoming request.
     *
     * @param          $request
     * @param callable $next
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            }

            return redirect()->action('HomeController@getIndex');
        }

        return $next($request);
    }
}
