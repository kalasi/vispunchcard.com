<?php
namespace App\Utilities;

/**
 * Class Time
 * @package App\Utilities
 */
class Time
{
    /**
     * Returns whether or not it is currently DST for the user
     *
     * @return boolean Whether or not it is currently DST
     */
    public static function isDST()
    {
        return date('I', time());
    }

    /**
     * Converts an integer-based timestamp into a readable string with hours:minutes:seconds
     *
     * @param int $i
     *
     * @return bool|string
     * @internal param int $int The integer timestamp to convert
     *
     */
    public static function short($i = 0)
    {
        return date('jS \of F Y', $i);
    }

    /**
     * Converts an integer-based timestamp into a readable string with hours:minutes:seconds
     *
     * @param int $i
     *
     * @return bool|string
     */
    public static function long($i = 0)
    {
        return date('jS \of F Y \- H:i:s', $i);
    }
}
