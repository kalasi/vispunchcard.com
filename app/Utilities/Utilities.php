<?php
namespace App\Utilities;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;

/**
 * Class Utilities
 * @package App\Utilities
 */
class Utilities
{
    /**
     * Outputs an encoded and parsed URL.
     *
     * @param  string $url The URL
     *
     * @return string      The URL-encoded and parsed URL
     */
    public static function URL($url = '')
    {
        $str = '/';

        return str_replace(' ', '%20', $str . $url);
    }

    /**
     * Outputs an encoded and parsed URL for business URLs.
     *
     * @param        $id
     * @param string $suffix
     *
     * @return string The URL-encoded and parsed URL
     */
    public static function URLBusiness($id, $suffix = '')
    {
        $business = \DB::table('businesses')
            ->select('name')
            ->where('id', $id)
            ->first();

        $str = "";

        if ($business) {
            $str = 'checkout/' . $id . '/' . urlencode($business->name);

            if (!empty($suffix)) {
                $str .= '/' . $suffix;
            }
        }

        return self::URL($str);
    }

    /**
     * Returns a string of the URL with the http/s and protocol included.
     *
     * @return string String of full URL with http/s and protocol included
     */
    public static function fullURL()
    {
        return 'http://' . $_SERVER['SERVER_NAME'] .
            ($_SERVER['SERVER_PORT'] != '80' ? ':' . $_SERVER['SERVER_PORT'] : '') . $_SERVER['REQUEST_URI'];
    }
    /**
     * Returns a true or false boolean of whether or not the user agent is a mobile.
     *
     * @return boolean
     */
    public static function mobile()
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i",$_SERVER["HTTP_USER_AGENT"]);
    }

    /**
     * Parses a string for converting gt, lt, double/single-quote characters into their ASCII alternatives.
     *
     * @param $str
     *
     * @return mixed
     */
    public static function clean($str)
    {
        $from = ['<', '>', '\'', '\"'];
        $to = ['&lt;', '&gt;', '&#39;', '&#34;'];

        return str_replace($from, $to, $str);
    }

    /**
     * Determines whether a string begins with a substring.
     *
     * @param  string $needle    The substring to search a larger string for
     * @param  string $haystack  The larger string to search within
     *
     * @return boolean           Whether a string begins with a substring
     */
    public static function startsWith($needle='',$haystack='')
    {
        return substr($haystack, 0, strlen($needle)) === $needle;
    }

    /**
     * Determines whether a string ends with a substring.
     *
     * @param  string $needle    The substring to search a larger string for
     * @param  string $haystack  The larger string to search within
     *
     * @return boolean           Whether a string ends with a substring
     */
    public static function endsWith($needle='',$haystack='')
    {
        return substr($haystack, -strlen($needle)) === $needle;
    }

    /**
     * Returns a string of a dollar value converted from pennies.
     *
     * @param  int  $cents               The amount of cents to convert
     * @param  boolean $roundIfNoPennies If true, rounds .00 to nothing; if false, does not
     *
     * @return string                    The dollar value in string, such as: $10.17 OR $10
     */
    public static function dollar($cents, $roundIfNoPennies = true)
    {
        $dollars = number_format($cents / 100, 2);

        if ($roundIfNoPennies) {
            $dollars = !self::endsWith('00', $dollars) ?: round($dollars);
        }

        return "\$" . $dollars;
    }
    /**
     * Returns a string value of a price amount in dollars and cents.
     *
     * @param  int $amount The amount in cents
     *
     * @return string         The price amount in dollars and cents
     */
    public static function price($amount)
    {
        $price = explode(".", $amount / 100);

        return "$" . $price[0] . "<sup>" . $price[1] . "</sup>";
    }

    /**
     * Returns whether or not it is currently DST for the user.
     *
     * @return boolean    Whether or not it is currently DST
     */
    public static function isDST()
    {
        return date("I", time());
    }

    /**
     * Converts an integer-based timestamp into a readable string with hours:minutes:seconds.
     *
     * @param int $i
     *
     * @return bool|string
     *
     */
    public static function timeLong($i = 0)
    {
        return date("jS \of F Y \- H:i:s", $i);
    }

    /**
     * Finds the difference between two strings.
     *
     * @param  array|string $first  First string
     * @param  array|string $second Second string
     *
     * @return array          The HTML difference between two strings
     */
    public static function diff($first,$second)
    {
        $matrix = [];

        $maxlen = 0;

        $nmax = 0;
        $omax = 0;

        foreach ($first as $oindex => $ovalue) {
            $nkeys = array_keys($second, $ovalue);

            foreach ($nkeys as $nindex) {
                $matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ?
                    $matrix[$oindex - 1][$nindex - 1] + 1 :
                    1;

                if ($matrix[$oindex][$nindex] > $maxlen) {
                    $maxlen = $matrix[$oindex][$nindex];
                    $omax = $oindex + 1 - $maxlen;
                    $nmax = $nindex + 1 - $maxlen;
                }
            }
        }

        if ($maxlen == 0) {
            return [['d' => $first, 'i' => $second]];
        }

        return array_merge(
            self::diff(
                array_slice($first, 0, $omax),
                array_slice($second, 0, $nmax)
            ),
            array_slice($second, $nmax, $maxlen),
            self::diff(
                array_slice($first, $omax+ $maxlen),
                array_slice($second, $nmax+ $maxlen)
            )
        );
    }

    /**
     * Finds the difference between two strings.
     *
     * @param  string $first  First string
     * @param  string $second Second string
     *
     * @return string         The HTML difference between two strings
     */
    public static function htmlDiff($first,$second)
    {
        $ret = '';

        $diff = self::diff(preg_split("/[\s]+/", $first), preg_split("/[\s]+/", $second));

        foreach ($diff as $k) {
            if (is_array($k)) {
                $ret .= (!empty($k['d']) ? "<b class='text-danger'>" . implode(' ', $k['d']) . "</b> " : '') .
                    (!empty($k['i']) ? "<b class='text-success'>" . implode(' ', $k['i']) . "</b> " : '');
            } else {
                $ret .= $k.' ';
            }
        }

        return $ret;
    }
    /**
     * Returns an array of the area, exchange, and sub values of a phone number.
     *
     * @param  string $phone The phone number to be processed into an array
     *
     * @return array         The processed phone number as an array [area,exchange,sub]
     */
    public static function processPhone($phone)
    {
        $from = ['(', ')', ' '];
        $to = ['', '', ''];

        $phone = str_ireplace($from, $to, $phone);

        $area = substr($phone, 0, 3);
        $exchange = substr($phone, 3, 3);
        $sub = substr($phone, 7, 4);

        $data = [
            'area'     => (int) $area,
            'exchange' => (int) $exchange,
            'sub'      => (int) $sub,
        ];

        return $data;
    }
    /**
     * Iterates through an array and all multidimensional keys of the array
     * to determine whether a needle is in any of the array/s.
     *
     * @param  string $needle    The needle to determine whether is in the array
     * @param  array $haystack   The array to search through for the $needle
     *
     * @return boolean           Whether or not the $needle is in the array or its subarrays.
     */
    public static function inArrayRecursive($needle,$haystack){
        $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($haystack));

        foreach ($iterator as $key => $element) {
            if ($element == $needle || $key == $needle) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether or not the version of the website that the client is using is good.
     *
     * @param $version
     *
     * @return bool
     */
    public static function versionGood($version)
    {
        $c = \DB::table('site')
            ->select('value')
            ->where('key','version')
            ->pluck('value');

        $c = explode(".", $c);
        $v = explode(".", $version);

        if ($v[0] == $c[0]) {
            if ($v[1] == $c[1]) {
                return true;
            }
        }

        return false;
    }
}
