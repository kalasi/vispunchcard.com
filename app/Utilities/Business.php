<?php
namespace App\Utilities;

/**
 * Class Business
 * @package App\Utilities
 */
class Business
{
    /**
     * Returns all of the information about a business by its ID, retrieved from the database
     *
     * @param  integer $id The ID of a business to retrieve information from the database about
     *
     * @return object      The object containing information about the business from the database
     */
    public static function info($id)
    {
        return \DB::table('businesses')
            ->select('*')
            ->where('id', $id)
            ->first();
    }

    /**
     * @param $id
     *
     * @return \stdClass
     */
    public static function permissions($id)
    {
        $business = Business::info($id);

        $permissions = new \stdClass;

        if ($business) {
            $isOwner = $business->owner == \User::$id;

            $isAdmin = \DB::table('admins')
                ->select('id')
                ->where('user',\User::$id)
                ->where('business',$id)
                ->first();

            if ($isOwner || $isAdmin) {
                $permissions->owner = $isOwner;
                $permissions->admin = !empty($isAdmin);
                $permissions->permissions = true;
            }

            $pin = \Cookie::get('u' . \User::$id . 'b' . $id . 'p');

            if ($pin == $business->pin) {
                $permissions->pin = true;
            } else {
                $permissions->pin = false;
            }
        }

        return $permissions;
    }

    /**
     * @return \Illuminate\View\View
     */
    public static function invalidPermissions()
    {
        $page = \View::make('errors.manage.invalidPermissions');

        $page->with('title', 'Error Managing Business');
        $page->with('titleSub', 'Not Owner');

        return $page;
    }

    public static function matchingRole($type)
    {
        $roles = \Database::getRoles();

        $displayRole = null;

        foreach ($roles as $role) {
            if ($type == $role->name_trim) {
                $displayRole = $role;
            }
        }

        return $displayRole;
    }

    /**
     * @return bool|object
     */
    public static function cookie()
    {
        $cookie = \Cookie::get('business-info');

        if ($cookie) {
            $cookie = unserialize(\Crypt::decrypt($cookie));

            $ret = (object) [
                'id'  => $cookie->id,
                'pin' => $cookie->pin,
            ];
        } else {
            return false;
        }

        return $ret;
    }

    /**
     * @return bool
     */
    public static function isBusiness()
    {
        $cookie = self::cookie();

        $isBusiness = false;

        if ($cookie) {
            $businessInfo = \DB::table('businesses')
                ->select('id')
                ->where('id', $cookie->id)
                ->where('pin', $cookie->pin)
                ->first();

            if ($businessInfo) {
                $isBusiness = true;
            }
        }

        return $isBusiness;
    }

    /**
     * @return bool
     */
    public static function signedIn()
    {
        $cookie = \Cookie::get('signed-in');

        if ($cookie) {
            $cookie = (object) \Crypt::decrypt(unserialize(\Crypt::decrypt($cookie)));

            if ($cookie) {
                $user = \DB::table('users')
                    ->select('id')
                    ->where('area', $cookie->area)
                    ->where('exchange', $cookie->exchange)
                    ->where('sub', $cookie->sub)
                    ->first();

                if ($user) {
                    return $user->id;
                }
            }
        }

        return false;
    }

    /**
     * @return mixed|null|static
     */
    public static function signedInInfo()
    {
        $cookie = \Cookie::get('signed-in');

        if ($cookie) {
            $cookie = (object) \Crypt::decrypt(unserialize(\Crypt::decrypt($cookie)));

            if ($cookie) {
                $user = \DB::table('users')
                    ->select('*')
                    ->where('area', $cookie->area)
                    ->where('exchange', $cookie->exchange)
                    ->where('sub', $cookie->sub)
                    ->first();

                return $user;
            }
        }

        return null;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public static function isStaff($id)
    {
        $isOwner = \DB::table('businesses')
            ->select('id')
            ->where('owner', $id)
            ->first();
        $isAdmin = \DB::table('admins')
            ->select('id')
            ->where('user', $id)
            ->where('business', self::cookie()->id)
            ->first();
        $isEmployee = \DB::table('employees')
            ->select('id')
            ->where('user', $id)
            ->where('business', self::cookie()->id)
            ->first();

        if ($isOwner || $isEmployee || $isAdmin) {
            return true;
        }

        return false;
    }
}
