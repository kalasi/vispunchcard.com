<?php
namespace App\Utilities;

/**
 * Class Database
 * @package App\Utilities
 */
class Database
{
    /**
     * @return array|static[]
     */
    public static function getRoles()
    {
        return \DB::table('roles')
            ->select('*')
            ->get();
    }
}
