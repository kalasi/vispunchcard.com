<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package App
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * @var string
     */
    public static $email = "";

    /**
     * @var string
     */
    public static $fname = "";

    /**
     * @var string
     */
    public static $id = "";

    /**
     * @var bool
     */
    public static $logged = false;

    /**
     * @return string
     */
    public static function name()
    {
        return self::$fname;
    }

    /**
     * @return string
     */
    public static function type()
    {
        return \Cookie::get('user-type');
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Sets the static information regarding the user's account
     */
    public static function login()
    {
        self::$logged = false;

        if (\Auth::check()) {
            $userData = \DB::table('users')
                ->select('fname')
                ->where('id', \Auth::user()->id)
                ->first();

            self::$id = \Auth::user()->id;
            self::$email = \Auth::user()->email;
            self::$logged = true;
            self::$fname = $userData->fname;
        }
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * @param string $value
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    /**
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }
}
