function Checkout(){
	this.full=0;
	this.fullCurrently=0;
	this.per=0;
	this.started=0;
	this.setup=function setup(){
		this.full=parseInt($('#punchcards-full').html().replace(/\D/g,''));
		this.fullCurrently=this.full;
		this.per=parseInt($('.punchcard em').attr('rt-per'));
		this.started=$('.punchcard em').attr('rt-started');
		return true;
	}
	/**
	 * This method is fired when the event handler is called by a change in the "New Punches Today" input field
	 */
	this.newPunches=function newPunches(){
		/**
		 * The base string for jQuery selections
		 * @type {String}
		 */
		b='input[name=new]';
		val=$(b).val();
		//Alright let's remove negative numbers and make sure it's a value
		val.replace(/\D/g,'');
		$(b).val(val);
		if(val.length==0)val=0;

		newAmount=parseInt(this.started)+parseInt(val);
		proposed=newAmount-this.started;
		newPunched=newAmount%this.per;
		nowFull=this.full-this.getUsed()+Math.floor(newAmount/this.per);
		$('.punchcard div').html('');
		$('.punchcard small').eq(0).html("<i class='fa fa-long-arrow-right'></i><span>"+newPunched+"</span>/<span>"+this.per+"</span><i class='fa fa-long-arrow-left'></i>");
		this.updateFull();
		if(newAmount<this.per){
			for(x=1;x<=this.started;x++){
				$('.punchcard div').append("<span class='punch'></span>");
			}
			for(x=1;x<=proposed;x++){
				$('.punchcard div').append("<span class='punch-proposed'></span>");
			}
			for(x=newAmount;x<this.per;x++){
				$('.punchcard div').append("<span class='punch-empty'></span>");
			}
		}
		else{
			for(x=1;x<=newPunched;x++){
				$('.punchcard div').append("<span class='punch-proposed'></span>");
			}
			for(x=newPunched;x<this.per;x++){
				$('.punchcard div').append("<span class='punch-empty'></span>");
			}
		}
		$('.punchcard small').eq(0).stop().animate({
			'color':'green',
			'font-weight':'bold'
		},500,function(){
			$('.punchcard small').eq(0).stop().animate({
				'color':'#bababa',
				'font-weight':'none'
			},600,function(){
				$('.punchcard small').html("<span>"+newPunched+"</span>/<span>"+Checkout.per+"</span>");
			});
		});
	}
	this.getFull=function getFull(){
		used=parseInt($('input[name=used]').val().replace(/-/g,''));
		if(isNaN(used)){
			used=0;
		}
		return parseInt(this.full-used);
	}
	this.getUsed=function getUsed(){
		return parseInt($('input[name=used]').val());
	}
	this.getNew=function getNew(){
		val=parseInt($('input[name=new]').val());
		if(isNaN(val)){
			val=0;
		}
		return val;
	}
	/**
	 * This method is called when the event handler is fired by a change in the "Free Used Today" input field
	 */
	this.newUsed=function newUsed(){
		/**
		 * The base string for jQuery selections
		 * @type {String}
		 */
		b='input[name=used]';
		used=parseInt($(b).val().replace(/-/g,''));
		$(b).val(used);
		remaining=this.getFull();
		full=parseInt($('#punchcards-full').html().replace(/\D/g,''));
		this.updateFull();
		changed=this.getFull();
		punchesCurrent=parseInt(this.getNew())+parseInt(this.started);
		changed+=parseInt(Math.floor(punchesCurrent/this.per));
		changed=parseInt(changed);
		if(changed<0){
			this.errorShow();
		}
		else{
			this.errorHide();
		}
	}
	this.updateFull=function updateFull(){
		changed=this.getFull();
		punchesCurrent=parseInt(this.getNew())+parseInt(this.started);
		changed+=parseInt(Math.floor(punchesCurrent/this.per));
		changed=parseInt(changed);
		if(this.fullCurrently!=changed){
			if(changed!=this.full){
				if(changed>0){
					$('#punchcards-full').html("<s class='text-danger'>"+this.full+"</s> <span class='text-success'>"+changed+"</s>");
				}
				else if(changed<0){
					$('#punchcards-full').html("<s class='text-danger'>"+this.full+"</s> <span class='text-warning'>"+changed+"</s> <i class='text-warning fa fa-warning'></i>");
				}
			}
			else{
				$('#punchcards-full').html(this.full);
			}
			this.fullCurrently=changed;
		}
	}
	/**
	 * Method is called when an event handler is called on form submission
	 * @return {boolean} If the punchcards full is >= the number of punchcards used, then returns true; else false
	 */
	this.finished=function finished(){
		changed=this.getFull();
		punchesCurrent=parseInt(this.getNew())+parseInt(this.started);
		changed+=parseInt(Math.floor(punchesCurrent/this.per));
		if(changed>=0||changed=="")return true;
		return false;
	}
	/**
	 * Displays the error that there aren't enough available punchcards
	 */
	this.errorShow=function errorShow(){
		b='#used-error';
		$('#used-error span').html(0-remaining);
		/**
		 * Display the error for 150ms
		 */
		$(b).stop().animate({
			opacity:1
		},150,function(){
			/**
			 * Hide the error again for another 150ms
			 */
			$(b).stop().animate({
				opacity:0
			},150,function(){
				/**
				 * Display the error again for 3000ms then hide it by calling self::errorHide();
				 */
				$(b).stop().animate({
					opacity:1
				});
			});
		});
	}
	/**
	 * Hides the error that there aren't enough available punchcards
	 */
	this.errorHide=function errorHide(){
		$('#used-error').stop().animate({
			opacity:0
		});
	}
}
$(function(){
	Checkout=new Checkout();
	Checkout.setup();
	$('input[name=new]').bind("keyup",function(){
		Checkout.newPunches();
	});
	/**
	 * The bind for when the "Free Used Today" changes in value by either the right-hand side ticker or by input
	 */
	$('input[name=used]').bind("keyup",function(){
		Checkout.newUsed();
	});
	$('form').submit(function(e){
		successful=Checkout.finished();
		if(!successful){
			e.preventDefault();
		}
	});
	$('input[name=new]').focus();
});