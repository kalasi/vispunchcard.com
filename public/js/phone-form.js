function PhoneForm(){
	this.fieldID='#phone';
	this.fields=[];
	this.setup=function setup(fields){
		this.fields=fields;
		$(this.fieldID).focus();
	}
	this.change=function change(ev){
		kc=ev.keyCode;
		if(kc==16)return false;
		l=$(this.fieldID).val().length;
		v=$(this.fieldID).val();
		if(kc==8){
			switch(l){
				case 2:
					$(this.fieldID).val("");
					break;
				case 7:
					newVal=v.replace(/\)/g,'').replace(/ /g,'');
					$(this.fieldID).val(newVal)
					break;
				case 11:
					newVal=v.replace(/-/g,'');
					$(this.fieldID).val(newVal);
					break;
			}
		}
		else{
			switch(l){
				case 0:
					$(this.fieldID).val("(");
					break;
				case 4:
					$(this.fieldID).val(v+") ");
					break;
				case 9:
					if(kc!=8){
						$(this.fieldID).val(v+"-");
					}
					break;
			}
		}
	}
	this.check=function check(ev){
		if($(this.fieldID).val().length==14){
			if(this.fields!=undefined){
				$(this.fields[0]).focus();
			}
			else{
				$(this.fieldID).closest("form").submit();
			}
		}
	}
}
$(function(){
	$('#phone').keydown(function(ev){
		PhoneForm.change(ev);
	});
	$('#phone').keyup(function(ev){
		PhoneForm.check(ev);
	});
});