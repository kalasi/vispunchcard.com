function VisPunchcard(){
	this.name="VisPunchcard";
	this.validateEmail=function validEmail(email){
		list=$.ajax({
			type:'GET',
			url:"/ajax/gtld",
			data:'',
			dataType:'html',
			context:document.body,
			global:false,
			async:false,
			success:function(data){
				return data;
			}
		}).responseText;
		list=$.parseJSON(list);
		gtld=email.split('.');
		gtldl=gtld.length;
		gtlds=gtld[gtldl-1];
		valid=false;
		if(gtldl>1&&gtlds.length>0){
			if($.inArray(gtlds,list)!==-1){
				valid=true;
			}
		}
		return valid;
	}
}
function Form(){
	this.answers=[];
	this.business="";
	this.currentQuestion=0;
	this.header="";
	this.questions=0;
	this.questionList=[];
	/**
	 * Sets up the form by declaring all of the necessary fields such as the business name, the form header, and declaring all of the question fields
	 */
	this.setup=function setup(){
		this.business=VisPunchcard.name;
		this.header=$('form').parent().find('em').eq(0).attr('rt-header');
		this.questions=$('form div').length;
		hiddenInputs=$('form').find('> input');
		//Go through hidden inputs as answers
		for(i=0;i<hiddenInputs.length;i++){
			hi=hiddenInputs[i];
			this.answers[i]={'name':$(hi).attr('name'),'value':$(hi).val()};
		}
		questions=document.getElementsByTagName('form')[0].getElementsByTagName('div');
		y=questions.length;
		questionSet="<div questions>";
		/**
		 * Iterate through all the form divs to gather the question info
		 * @type {Number}
		 */
		for(x=0;x<y;x++){
			/**
			 * The base selector in jQuery to select fields in the question div
			 * @type {String}
			 */
			b='form div:eq('+x+') ';
			type='';
			if($(b+'input').length>0){
				input=$(b+'input');
				type='input';
			}
			else if($(b+'textarea').length>0){
				input=$(b+'textarea');
				type='textarea';
			}
			this.questionList[x]={
				'bind':$(b+'em').attr('rt-bind'),
				'input':input,
				'label':$(b+'span').html(),
				'info':$(b+'p:eq(0)'),
				'error':$(b+'p:eq(1)'),
				'required':$(b+'em').attr('rt-required'),
				'enter':$(b+'em').attr('rt-enter'),
				'placeholder':$(b+'em').attr('rt-placeholder'),
				'type':type
			};
			questionSet+="<div><span>"+this.questionList[x].label+"</span></div>";
		}
		questionSet+="</div>";
		$('form').parent().append(questionSet);
		newWidth=$('form').parent().children('div[questions]').width()/
			$('form').parent().children('div[questions]').children('div').length;
		$('form').
			parent().
			children('div[questions]').
			children('div').
			css('width',
				$('form').parent().children('div[questions]').width()/
				$('form').parent().children('div[questions]').children('div').length
			);
		$('form').parent().append('<s><b></b></s>');
		$('form').parent().prepend("<div links><i id='go-home' class='fa fa-home'></i> Home<br /><i id='go-back' class='fa fa-arrow-left'></i> Go Back One Page</div>");
		this.newQuestion();
		$('#go-back').click(function(){
			if(history.length===1){
				window.location="/";
			}
			else{
				history.back();
			}
		});
		$('#go-home').click(function(){
			window.location="/";
		});
	}
	/**
	 * Once the previous question has been done this is called by self::questionDone method to present the next question
	 */
	this.newQuestion=function newQuestion(){
		/**
		 * The current question object
		 * @type {Object}
		 */
		c=this.questionList[this.currentQuestion];
		placeholder="";
		if(c.placeholder!=""){
			placeholder=c.placeholder;
		}
		else{
			switch(c.bind){
				case "phone":
					placeholder="Enter Phone Number (987) 654-3210";
					break;
				case "pin":
					placeholder="ex. "+this.randomNumber(1000,9999);
					break;
				case "name":
					names=["John"];
					placeholder="ex. "+names[Math.floor(Math.random()*items.length)];
			}
		}
		/**
		 * Set the current question in the bottom
		 */
		$('div[questions]').children('div').removeAttr('rt-current-question');
		$('div[questions]').children('div').eq(this.currentQuestion).attr('rt-current-question','');
		html=
		"<h1>"+
			this.business+
		"</h1>"+
		"<h2>"+
			this.header+
		"</h2>"+
		"<h3>"+
			"<span>"+(this.currentQuestion+1)+"</span>/<span>"+(this.questions)+"</span>"+
		"</h3>";
		if(this.currentQuestion>0){
			html+=
		"<h4>"+
			"<i class='fa fa-chevron-left'></i> Back One Question"+
		"</h4>";
		}
		html+=
		"<div class='row'>"+
			"<div class='col-xs-12 col-sm-offset-4 col-sm-8'>"+
				"<b error>"+
					$(c.error).html()+
				"</b>"+
			"</div>"+
		"</div>"+
		"<div class='form-group'>"+
			"<label class='col-sm-4 control-label' for='"+$(c.input).attr('id')+"'>"+
				c.label+
			"</label>"+
			"<div class='col-sm-8'>"+
				$(c.input).prop('outerHTML')+
			"</div>"+
		"</div>"+
		"<div class='row'>"+
			"<div class='col-xs-12 col-sm-offset-4 col-sm-8'>"+
				"<p>"+
					$(c.info).html()+
				"</p>"+
			"</div>";
		if(c.enter=="1"){
			tag=$(c.input).prop('tagName');
			tag=tag.toLowerCase();
			console.log("Tag: "+tag);
			html+=
			"<div class='col-xs-12'>"+
				"<strong>";
			if(tag=="input"){
				html+=
					"Press ENTER to continue or click here";
			}
			else if(tag=="textarea"){
				html+=
					"Click here to continue";
			}
			html+=
				"</strong>"+
			"</div>";
		}
		html+=
		"</div>"+
		"<em rt-bind='"+c.bind+"'></em>";
		a=$('form').html(html);
		$('form input').attr('placeholder',placeholder);
		$('form input').css('background-image',"url('/img/icon/asterisk.svg')");
		$('form input').css('background-repeat','no-repeat');
		$('form input').css('background-position','right top');
		$('form input').css('background-size','29px');
		$('form input').attr('autocomplete','off');
		$('form '+c.type).focus();
		this.applyBinds();
		this.updateProgress(0,1);
	}
	/**
	 * Determines if there is another question to be presented once the previous was finished
	 */
	this.questionDone=function questionDone(){
		console.log("NEXT");
		c=this.questionList[this.currentQuestion];
		name=$('input').attr('name');
		val=$(c.type).val();
		index=this.answers.length;
		if(c.required=="1"){
			if(val.length<=0){
				return false;
			}
		}
		this.answers[index]={'name':name,'value':val};
		if(this.currentQuestion+1==this.questions){
			$('form').html(' ');
			for(x=0;x<this.answers.length;x++){
				ans=this.answers[x];
				text="<input type='hidden' name='"+ans.name+"' value='"+ans.value+"' style='display:none;' />";
				$('form').append(text);
			}
			$('form').append("<h5>Currently Submitting</h5><h6><i class='fa fa-spinner fa-spin'></i></h6>");
			$('form').submit();
		}
		else{
			this.currentQuestion+=1;
			this.newQuestion();
		}
	}
	this.questionPrevious=function questionPrevious(){
		this.currentQuestion--;
		console.log(this.currentQuestion);
		this.newQuestion();
	}
	/**
	 * Fires the event for a phone-binded input
	 * @param  {Object} ev The event object fired via the handler
	 */
	this.evPhone=function evPhone(ev){
		e=$('form input');
		v=$(e).val();
		l=v.length;
		kc=ev.keyCode;
		if(kc==16)return false;
		if(kc==10||kc==13) return false;
		if(kc!=8){
			switch(l){
				case 0:
					$(e).val("(");
					break;
				case 4:
					$(e).val(v+") ");
					break;
				case 9:
					$(e).val(v+"-");
			}
		}
		else{
			switch(l){
				case 1:
					$(e).val("(");
					break;
				case 2:
					$(e).val("");
					break;
				case 7:
					newVal=v.replace(/\)/g,'').replace(/ /g,'');
					$(e).val(newVal);
					break;
				case 11:
					newVal=v.replace(/-/g,'');
					$(e).val(newVal);
					break;
			}
		}
	}
	/**
	 * Fires the event for the handler of a pin-binded input
	 * @param  {Object} ev The event object sent via the handler
	 */
	this.evPin=function evPin(ev){
		e=$('form input');
		v=$(e).val();
		l=v.length;
		kc=ev.keyCode;
	}
	this.evName=function evName(ev){
	}
	this.evEmail=function evEmail(ev){
	}
	this.chPhone=function chPhone(ev){
		e=$('form input');
		v=$(e).val();
		l=v.length;
		this.updateProgress(l,14);
		if(l==14){
			this.questionDone();
		}
	}
	this.chPin=function chPin(ev){
		e=$('form input');
		v=$(e).val();
		l=v.length;
		//Remove non-numerics
		$(e).val(v.replace(/\D/g,''));
		this.updateProgress(l,4);
		if(l==4){
			this.questionDone();
		}
	}
	this.chName=function chName(ev){
		e=$('form input');
		v=$(e).val();
		l=v.length;
		kc=ev.keyCode;
		this.updateProgress(0,1);
		if(kc==13){
			this.questionDone();
		}
	}
	this.chEmail=function chEmail(ev){
		e=$('form input');
		v=$(e).val();
		l=v.length;
		kc=ev.keyCode;
		cp=0;
		if(v.indexOf('@')!==-1)cp++;
		if(v.indexOf('.')!==-1)cp++;
		//Determine if valid gTLD
		gtld=v.split('.');
		gtldl=gtld.length;
		isValid=VisPunchcard.validateEmail(v);
		if(isValid){
			this.questionDone();
		}
		this.updateProgress(cp,3);
	}
	this.updateProgress=function updateProgress(cur,tot){
		minP=0;
		maxP=0;
		curP=0;
		nowP=0;
		if(this.currentQuestion==0){
			minP=Math.round(100*((100*this.currentQuestion)/(100*this.questions)));
			maxP=Math.round(((this.currentQuestion+1)/this.questions)*100);
			curP=cur/tot;
			nowP=Math.round(maxP*curP);
		}
		else{
			minP=Math.round(100*((100*this.currentQuestion)/(100*this.questions)));
			maxP=Math.round(((this.currentQuestion+1)/this.questions)*100);
			curP=cur/tot;
			nowP=minP+((maxP-minP)*curP);
		}
		newWidth=Math.round(nowP*$('div.wrapper s').width()/100)+'px';
		console.log(newWidth);
		$('div.wrapper s b').stop().animate({
			'width':newWidth
		},350);
	}
	this.showError=function showError(){
		$('form b').css('visibility','visible');
	}
	/**
	 * Returns a random integer between the minimum and maximum numbers specified in the method call
	 * @param  {integer} min The minimum integer that the return integer can be
	 * @param  {integer} max The maximum integer that the return integer can be
	 * @return {integer} The return integer that is between the min and max integers
	 */
	this.randomNumber=function randomNumber(min,max){
		return Math.floor(Math.random()*(max-min+1))+min;
	}
	this.applyBinds=function applyBinds(){
		$('input').keydown(function(event){
			bind=$(event);
			bind=$(event.target).parent().parent().parent().find('em').attr('rt-bind');
			switch(bind){
				case "phone":
					Form.evPhone(event);
					break;
				case "pin":
					Form.evPin(event);
					break;
				case "name":
					Form.evName(event);
					break;
				case "email":
					Form.evEmail(event);
					break;
				case "integer":
					if(event.keyCode==13){
						Form.questionDone();
					}
					break;
			}
		});
		$('input').keyup(function(event){
			bind=$(event);
			bind=$(event.target).parent().parent().parent().find('em').attr('rt-bind');
			switch(bind){
				case "phone":
					Form.chPhone(event);
					break;
				case "pin":
					Form.chPin(event);
					break;
				case "name":
					Form.chName(event);
					break;
				case "email":
					Form.chEmail(event);
					break;
			}
		});
		$('form input').bind('keypress keydown keyup',function(e){
			if(e.keyCode==13){
				e.preventDefault();
			}
		});
		$('strong').click(function(){
			Form.questionDone();
		});
		$('h4').click(function(){
			Form.questionPrevious();
		});
	}
}
var VisPunchcard=new VisPunchcard();